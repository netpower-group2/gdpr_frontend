import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthorizationService } from '../_services/authorization.service';

@Injectable({
  providedIn: 'root'
})

export class AuthorizationGuard implements CanActivate, CanActivateChild {

  constructor (
    private authorziationService: AuthorizationService,
    private router: Router
  ){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean
  {
    const allowedRoles = next.data.allowedRoles;
    const isAuthorized = this.authorziationService.isAuthorized(allowedRoles);

    if (!isAuthorized) {
      this.router.navigate(['login']);
    }

    return isAuthorized;
  }

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean
  {
    const allowedRoles = next.data.allowedRoles;
    const isAuthorized = this.authorziationService.isAuthorized(allowedRoles);

    if (!isAuthorized) {
      this.router.navigate(['login']);
    }

    return isAuthorized;
  }
}
