import { Component, OnInit, Output, EventEmitter, ViewChild, TemplateRef } from '@angular/core';
import { PingJWTService } from './_services/ping-jwt.service'
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Router } from '@angular/router';
import { CrudService } from './_services/crud.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'GDPR';
  timeJwtNotification: number;

  @ViewChild(TemplateRef, { static: true }) template?: TemplateRef<{}>;

  constructor(
    private pingJWTService: PingJWTService,
    private notificationService: NzNotificationService,
    private router: Router
  ) { }

  // 1000*60*20
  x = setInterval(() => {
    this.timeJwtNotification = this.pingJWTService.pingJWT();
    this.expiredJWT();
  }, 1000*60*20)


  expiredJWT(): void {
    this.notificationService.template(this.template!, { nzDuration: 0 },);
  }

  btnClick() {
    localStorage.removeItem('userRole');
    this.router.navigate(['/login']);
    this.notificationService.remove();
  }

}

