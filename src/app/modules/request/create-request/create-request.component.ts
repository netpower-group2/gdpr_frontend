import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import * as moment from 'moment';
import { RequestModel } from 'src/app/_models/request.model';
import { Server } from 'src/app/_models/server.model';
import { ServerSelect } from 'src/app/_models/serverSelect.model';
import { Customer } from '../../../_models/customer.model';
import { RequestService } from '../service/request.service';
@Component({
  selector: 'app-create-request',
  templateUrl: './create-request.component.html',
  styleUrls: ['./create-request.component.less']
})
export class CreateRequestComponent implements OnInit {
  @Output() createRequest = new EventEmitter<RequestModel>();
  listServer: ServerSelect[] = [];
  isVisible = false;
  validateForm: FormGroup;
  showModal(): void {
    this.isVisible = true;
  }
  handleCancel(): void {
    this.isVisible = false;
    this.validateForm.reset();
    this.validateForm = this.fb.group({
      title: ['', [Validators.required]],
      fromDate: ['', [Validators.required]],
      toDate: ['', [Validators.required]],
      serverId: ['', [Validators.required]],
      description: [''],
    });
    this.endOpen = false;
  }
  constructor(
    private fb: FormBuilder,
    private requestService: RequestService
  ) {
    this.validateForm = this.fb.group({
      title: ['', [Validators.required]],
      fromDate: ['', [Validators.required]],
      toDate: ['', [Validators.required]],
      serverId: ['', [Validators.required]],
      description: [''],
    });
  }

  ngOnInit(): void {
    this.requestService.getAllServer().subscribe(res => {
      if (res.success) {
        this.listServer = res.serverDetails.map(s => new ServerSelect(
          s.serverId,
          s.nameServer,
          s.ipAddress
        ));
      }
    });
  }

  submitForm(value: { title: string; fromDate: string; toDate: string; serverId: string; description: string }): void {
    for (const key in this.validateForm.controls) {
      this.validateForm.controls[key].markAsDirty();
      this.validateForm.controls[key].updateValueAndValidity();
    }
    value.fromDate = moment(value.fromDate).format()
    value.toDate = moment(value.toDate).format()
    this.requestService.createRequest(value).subscribe(res => {
      if (res.success) {
        const newRequest = new RequestModel(
          res.resultCreate.createdByName,
          res.resultCreate.createdDate,
          res.resultCreate.updatedByName,
          res.resultCreate.updatedDate,
          res.resultCreate.description,
          res.resultCreate.fromDate,
          res.resultCreate.requestId,
          res.resultCreate.numId,
          res.resultCreate.serverId,
          res.resultCreate.serverIpAddress,
          res.resultCreate.serverName,
          res.resultCreate.status,
          res.resultCreate.title,
          res.resultCreate.toDate,
          res.resultCreate.answer
        );
        this.createRequest.emit(newRequest);
      }
      else { this.createRequest.emit(null); }
    });

    this.isVisible = false;
    this.validateForm.reset();
    this.validateForm = this.fb.group({
      title: ['', [Validators.required]],
      fromDate: ['', [Validators.required]],
      toDate: ['', [Validators.required]],
      serverId: ['', [Validators.required]],
      description: [''],
    });
  }

  startValue: Date | null = null;
  endValue: Date | null = null;
  endOpen = false;

  disabledStartDate = (startValue: Date): boolean => {
    if (!startValue || !this.endValue) {
      return false;
    }
    return startValue.getTime() > this.endValue.getTime();
  };

  disabledEndDate = (endValue: Date): boolean => {
    if (!endValue || !this.startValue) {
      return false;
    }
    return endValue.getTime() <= this.startValue.getTime();
  };

  onStartChange(date: Date): void {
    this.startValue = date;
  }

  onEndChange(date: Date): void {
    this.endValue = date;
  }

  handleStartOpenChange(open: boolean): void {
    if (!open) {
      this.endOpen = true;
    }
  }

  handleEndOpenChange(open: boolean): void {
    this.endOpen = open;
  }

}
