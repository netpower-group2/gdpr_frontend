import { Component, OnInit } from '@angular/core';
import { RequestModel } from '../../../_models/request.model';
import { Router, ActivatedRoute } from '@angular/router';
import { RequestService } from '../service/request.service';
import { FADE_CLASS_NAME_MAP } from 'ng-zorro-antd/modal';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import * as moment from 'moment';
import { JwtHelperService } from '@auth0/angular-jwt';
@Component({
  selector: 'app-request',
  templateUrl: './list-request.component.html',
  styleUrls: ['./list-request.component.less']
})
export class ListRequestComponent implements OnInit {
  visibleCreated = false;
  visibleUpdated = false;
  visibleServerIP = false;
  searchValue: string = '';
  searchCreatedByValue: string = '';
  searchUpdatedByValue: string = '';
  searchServerIPValue: string = '';
  searchColumn: string = '';
  checked = false;
  isLoading = false;
  indeterminate = false;
  setOfCheckedId = new Set<string>();
  listRequests: RequestModel[] = [];
  pageIndex = 1;
  pageSize = 10;
  sortOrder = '';
  sortField = '';
  total: number;
  rol: number;

  filterStatus = [
    { text: 'New', value: '2' },
    { text: 'Approved', value: '3' },
    { text: 'Closed', value: '4' },
    { text: 'Denied', value: '5' }
  ]

  filter: Array<{ key: string, value: string[] }>;

  constructor(
    private requestService: RequestService,
    private router: Router,
    private jwtHelperService: JwtHelperService
  ) { }

  ngOnInit(): void {
    this.getRequests(this.pageIndex, this.pageSize, null, null, []);
    this.rol = this.jwtHelperService.decodeToken(JSON.parse(localStorage.getItem('userRole')).authToken).rol
  }

  updateCheckedSet(id: string, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  addNewRequest(event: RequestModel): void {
    if (event != null) {
      this.listRequests = [event].concat(this.listRequests);
      if (this.listRequests.length > this.pageSize) {
        var x = this.listRequests.pop();
        this.setOfCheckedId.delete(x.requestId);
      }
      this.refreshCheckedStatus();
    }
  }

  onEditObjectAction(requestId: string): void {
    this.router.navigate([`request/manage/${requestId}`]);
  }

  onCurrentPageDataChange(listOfCurrentPageData: RequestModel[]): void {
    this.listRequests = listOfCurrentPageData;
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    this.checked = this.listRequests.every(item => this.setOfCheckedId.has(item.requestId));
    this.indeterminate = this.listRequests.some(item => this.setOfCheckedId.has(item.requestId)) && !this.checked;
  }

  onItemChecked(id: string, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  onAllChecked(value: boolean): void {
    this.listRequests.forEach(item => this.updateCheckedSet(item.requestId, value));
    this.refreshCheckedStatus();
  }



  getRequests(
    pageIndex: number,
    pageSize: number,
    sortField: string | null,
    sortOrder: string | null,
    filter: Array<{ key: string; value: string[] }>,
    searchColumn: string = null,
    searchKey: string = null
  ): void {
    this.isLoading = true;
    this.requestService.getRequestsPaging(pageIndex, pageSize, sortField, sortOrder, filter, searchColumn, searchKey).subscribe(res => {
      this.isLoading = false;
      if (res.success) {
        this.total = res.noRecords;
        this.listRequests = res.requestDetails.map(eachRequest => new RequestModel(
          eachRequest.createdByName,
          eachRequest.createdDate,
          eachRequest.updatedByName,
          eachRequest.updatedDate,
          eachRequest.description,
          eachRequest.fromDate,
          eachRequest.requestId,
          eachRequest.numId,
          eachRequest.serverId,
          eachRequest.serverIpAddress,
          eachRequest.serverName,
          eachRequest.status,
          eachRequest.title,
          eachRequest.toDate,
          eachRequest.answer
        ));
        this.setOfCheckedId.clear();
        this.refreshCheckedStatus();
        if (res.noRecords <= 10) {
          this.pageIndex = 1;
        }
      }
    });
  }


  onQueryParamsChange(params: NzTableQueryParams): void {
    this.pageSize = params.pageSize;
    this.pageIndex = params.pageIndex;
    const currentSort = params.sort.find(item => item.value !== null);
    this.sortField = (currentSort && currentSort.key) || null;
    this.sortOrder = (currentSort && currentSort.value) || null;
    this.filter = params.filter;
    this.getRequests(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter, this.searchColumn, this.searchValue);
  }

  searchServerIP(): void {
    this.setOfCheckedId.clear();
    this.visibleServerIP = false;
    this.searchColumn = 'Server';
    this.searchValue = this.searchServerIPValue;
    this.getRequests(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter, this.searchColumn, this.searchValue);
  }

  searchUpdatedBy(): void {
    this.setOfCheckedId.clear();
    this.visibleUpdated = false;
    this.searchColumn = 'UpdatedDate';
    this.searchValue = this.searchUpdatedByValue;
    this.getRequests(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter, this.searchColumn, this.searchValue);
  }

  searchCreatedBy(): void {
    this.setOfCheckedId.clear();
    this.visibleCreated = false;
    this.searchColumn = 'CreatedDate';
    this.searchValue = this.searchCreatedByValue;
    this.getRequests(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter, this.searchColumn, this.searchValue);
  }

  multipleAction(status: number): void {
    const arrId = Array.from(this.setOfCheckedId);
    let lstId = '';
    if (status === 3) {
      arrId.forEach(element => {
        const checkRequest = this.listRequests.find(x => x.requestId == element);
        if (checkRequest.status === 2) {
          lstId = lstId + ',' + element;
        }
      });
      if (lstId !== '') {
        lstId = lstId.substring(1);
      }
    }
    else {
      lstId = arrId.join(',');
    }

    this.requestService.multipleChange(status, lstId).subscribe(res => {
      if (res.success) {
        this.getRequests(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
      }
    });

    this.setOfCheckedId.clear();
    this.refreshCheckedStatus();

  }

  reset(): void {
    if (this.searchCreatedByValue !== '') {
      this.searchCreatedByValue = '';
    }
    if (this.searchUpdatedByValue !== '') {
      this.searchUpdatedByValue = '';
    }
    if (this.searchServerIPValue !== '') {
      this.searchServerIPValue = '';
    }
    this.searchValue = '';
    this.getRequests(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
  }
}
