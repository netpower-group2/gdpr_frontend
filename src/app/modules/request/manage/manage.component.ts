import { Component, OnInit } from '@angular/core';
import { formatDistance } from 'date-fns';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ActivatedRoute } from '@angular/router';
import { RequestService } from '../service/request.service';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { HistoryLog } from '../../../_models/historyLog.model';
import endOfMonth from 'date-fns/endOfMonth';

import * as moment from 'moment';
import { ServerSelect } from 'src/app/_models/serverSelect.model';
@Component({
  selector: 'app-request-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.less']
})
export class RequestManageComponent implements OnInit {
  constructor(
    private requestService: RequestService,
    private route: ActivatedRoute,
    private notification: NzNotificationService,
    private jwtHelperService: JwtHelperService,
  ) { }
  requestDetails = {
    requestId: '',
    numId: '',
    status: '',
    createdDate: '',
    createdBy: '',
    updatedDate: '',
    updatedBy: '',
    title: '',
    fromDate: '',
    toDate: '',
    serverId: '',
    serverName: '',
    serverIpAddress: '',
    description: '',
    answer: '',
    createdById: ''
  };
  requestId: string;
  listComment = [];
  listHistoryLog: HistoryLog[] = [];
  listServer: ServerSelect[] = [];
  isVisible = false;
  getParentCommentTimes = 1;
  getChildCommentTimes = 1;
  numberOfParentComments: number;
  numberOfChildrenComments: number;
  submitting = false;
  user = {
    id: JSON.parse(localStorage.getItem('userRole')).id,
    name: '',
    avatar: '',
    rol: this.jwtHelperService.decodeToken(JSON.parse(localStorage.getItem('userRole')).authToken).rol
  };
  inputReply = '';
  inputComment = '';
  clickReply = false;
  idParentComment = '';
  startValue: Date | null = null;
  endValue: Date | null = null;
  endOpen = false;

  loves = 0;
  time = formatDistance(new Date(), new Date());


  isCollapsed = false;
  love(idChild: string, idParent: string): void {
    if (idChild == null){
      const loveComment = this.listComment.find(x => x.id === idParent);
      if (loveComment.isLoved) {
        loveComment.isLoved = 0;
        this.requestService.love(idParent, 0).subscribe();
      }
      else {
        loveComment.isLoved = 1;
        this.requestService.love(idParent, 1).subscribe();
      }
    }
    else {
      const loveComment = this.listComment.find(x => x.id === idParent);
      let loveChildComment;
      for (const i of loveComment.children){
        if (i.id === idChild) {
          loveChildComment = i;
        }
      }
      if (loveChildComment.isLoved) {
        loveChildComment.isLoved = 0;
        this.requestService.love(loveChildComment.id, 0).subscribe();
      }
      else {
        loveChildComment.isLoved = 1;
        this.requestService.love(loveChildComment.id, 1).subscribe();
      }
    }
  }

  disabledStartDate = (startValue: Date): boolean => {
    if (!startValue || !this.endValue) {
      return false;
    }
    return startValue.getTime() > this.endValue.getTime();
  }

  disabledEndDate = (endValue: Date): boolean => {
    if (!endValue || !this.startValue) {
      return false;
    }
    return endValue.getTime() <= this.startValue.getTime();
  }

  onStartChange(date: Date): void {
    this.startValue = date;
  }

  onEndChange(date: Date): void {
    this.endValue = date;
  }

  handleStartOpenChange(open: boolean): void {
    if (!open) {
      this.endOpen = true;
    }
  }

  handleEndOpenChange(open: boolean): void {
    this.endOpen = open;
  }

  reply(idComment: string): void {
    if (!this.clickReply) {
      this.clickReply = true;
      this.idParentComment = idComment;
      this.getChildCommentTimes = 1;
      this.getListChildrenComment();
    }
    else {
      this.clickReply = false;
      const pComment = (this.listComment.find(x => x.id === this.idParentComment));
      pComment.children = [];
      this.numberOfChildrenComments = 0;
    }
  }

  handleSubmit(idCommentReply: string): void {
    this.submitting = true;
    let content = '';
    if (this.inputComment === '') {
      content = this.inputReply;
      this.inputReply = '';
    }
    else {
      content = this.inputComment;
      this.inputComment = '';
    }
    setTimeout(() => {
      this.submitting = false;
    }, 800);

    const newComment =
    {
      requestId: this.requestDetails.requestId,
      replyFor: idCommentReply,
      content
    };

    this.requestService.createComment(newComment.requestId, newComment.replyFor, newComment.content).subscribe(res => {
      if (idCommentReply === '') {
        if (this.listComment.length === this.getParentCommentTimes * 10) {
          this.getParentCommentTimes = this.getParentCommentTimes + 1;
        }
        this.requestService.getCommentListPaging(this.requestDetails.requestId, this.getParentCommentTimes, 10, '')
          .subscribe(resComment => {
            for (let x = 0; x < resComment.commentDetails.length - 1; x = x + 1) {
              this.listComment.pop();
            }
            this.listComment.push(...resComment.commentDetails);
            this.numberOfParentComments = resComment.numberOfComments;
          });
      }
      else {
        const pComment = this.listComment.find(x => x.id === this.idParentComment);
        if (pComment.children.length === this.getChildCommentTimes * 10) {
          this.getChildCommentTimes = this.getChildCommentTimes + 1;
        }
        pComment.numberOfChild = pComment.numberOfChild + 1;
        this.requestService.getCommentListPaging(this.requestDetails.requestId, this.getChildCommentTimes, 10, this.idParentComment)
          .subscribe(resComment => {
            for (let x = 0; x < resComment.commentDetails.length - 1; x = x + 1) {
              pComment.children.pop();
            }
            pComment.children.push(...resComment.commentDetails);
            this.numberOfChildrenComments = resComment.numberOfComments;
          });
      }
    });
  }

  showModal(): void {
    this.getListParentComment();
    this.isVisible = true;
  }

  getListParentComment(): void {
    this.requestService.getCommentListPaging(this.requestDetails.requestId, this.getParentCommentTimes, 10, '').subscribe(res => {
      this.listComment.push(...res.commentDetails);
      this.numberOfParentComments = res.numberOfComments;
    });
  }

  getListChildrenComment(): void {
    this.requestService.getCommentListPaging(this.requestDetails.requestId, this.getChildCommentTimes, 10, this.idParentComment)
      .subscribe(res => {
        const pComment = (this.listComment.find(x => x.id === this.idParentComment));
        pComment.children.push(...res.commentDetails);
        this.numberOfChildrenComments = res.numberOfComments;
      });
  }

  readmoreParent(): void {
    this.getParentCommentTimes = this.getParentCommentTimes + 1;
    this.getListParentComment();
  }

  readmoreChildren(): void {
    this.getChildCommentTimes = this.getChildCommentTimes + 1;
    this.getListChildrenComment();
  }

  normalizeDate(date: string): string {
    return moment(date).format('DD.MM.YYYY - h:mmA');
  }

  handleOk(): void {
    this.isVisible = false;
    this.clickReply = false;
    this.listComment = [];
  }

  handleCancel(): void {
    this.isVisible = false;
    this.clickReply = false;
    this.listComment = [];
  }

  updateRequest(): void {
    const update = {
      title: this.requestDetails.title,
      fromDate: this.requestDetails.fromDate,
      toDate: this.requestDetails.toDate,
      serverId: this.requestDetails.serverId,
      description: this.requestDetails.description
    };
    this.requestService.updateRequest(this.requestDetails.requestId, update).subscribe(res => {
      this.updateSuccessNotification('success');
      this.getHistoryLogs();
    });
  }

  approve(): void {
    this.requestService.manageRequest(this.requestDetails.answer, 3, this.requestDetails.requestId).subscribe(res => {
      this.approveSuccessNotification('success');
      this.getInfoRequest();
      this.getHistoryLogs();
    });
  }

  deny(): void {
    this.requestService.manageRequest(this.requestDetails.answer, 5, this.requestDetails.requestId).subscribe(res => {
      this.denySuccessNotification('success');
      this.getInfoRequest();
      this.getHistoryLogs();
    });
  }

  close(): void {
    this.requestService.manageRequest(this.requestDetails.answer, 4, this.requestDetails.requestId).subscribe(res => {
      this.closeSuccessNotification('success');
      this.getInfoRequest();
      this.getHistoryLogs();
    });
  }

  getInfoRequest(): void {
    this.route.params.subscribe(params => {
      this.requestId = params.id;

      if (this.requestId) {
        this.requestService.getRequestByRequestId(this.requestId).subscribe(res => {
          const resDetails = res.requestDetails;
          this.requestDetails.requestId = resDetails.requestId;
          this.requestDetails.numId = resDetails.numId;
          this.requestDetails.status = resDetails.status === 2 ? 'New' : resDetails.status === 3 ? 'Approved' : resDetails.status === 4 ? 'Closed' : resDetails.status === 5 ? 'Denied' : '';
          this.requestDetails.createdDate = this.normalizeDate(resDetails.createdDate);
          this.requestDetails.createdBy = resDetails.createdByName;
          this.requestDetails.updatedDate = resDetails.updatedDate == null ? null : this.normalizeDate(resDetails.updatedDate);
          this.requestDetails.updatedBy = resDetails.updatedByName;
          this.requestDetails.title = resDetails.title == null ? '' : resDetails.title;
          this.requestDetails.fromDate = resDetails.fromDate;
          this.requestDetails.toDate = resDetails.toDate;
          this.requestDetails.serverId = resDetails.serverId;
          this.requestDetails.serverName = resDetails.serverName;
          this.requestDetails.serverIpAddress = resDetails.serverIpAddress;
          this.requestDetails.description = resDetails.description;
          this.requestDetails.createdById = resDetails.createdBy;
        });

        this.requestService.getAllServer().subscribe(res => {
          if (res.success) {
            this.listServer = res.serverDetails.map(s => new ServerSelect(
              s.serverId,
              s.nameServer,
              s.ipAddress
            ));
          }
        });
      }
    });
  }

  getHistoryLogs(): void {
    this.route.params.subscribe(params => {
      this.requestId = params.id;
      if (this.requestId) {
        this.requestService.getHistoryLog(this.requestId).subscribe(res => {
          this.listHistoryLog = res.historyLogDetails.map(h => new HistoryLog(
            h.modifiedDate == null ? null : this.normalizeDate(h.modifiedDate),
            h.modifier,
            h.fileContent,
            h.logContent
          ));
        }
        );
      }
    });
  }

  updateSuccessNotification(type: string): void {
    this.notification.create(
      type,
      'Updated!',
      'Saved saved saved.'
    );
  }

  approveSuccessNotification(type: string): void {
    this.notification.create(
      type,
      'Approved!',
      'Server is openning for this request.'
    );
  }

  denySuccessNotification(type: string): void {
    this.notification.create(
      type,
      'Denied!',
      'You have denied this request.'
    );
  }

  closeSuccessNotification(type: string): void {
    this.notification.create(
      type,
      'Closed!',
      'This request is closed.'
    );
  }

  ngOnInit(): void {
    this.getInfoRequest();
    this.getHistoryLogs();
  }
}
