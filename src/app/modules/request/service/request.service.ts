import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RequestBaseService } from '../../../_services/baserequest.service';
import { RequestModel } from '../../../_models/request.model';
import { PageRequestResponse } from '../../../_models/pagerequest-response';
import { CreateRequestResponse } from '../../../_models/createrequest-response';
import { UpdateResponse } from 'src/app/_models/update-response';
import { ServerSelect } from 'src/app/_models/serverSelect.model';
import { PageServerResponse } from 'src/app/_models/pageserver-response';

@Injectable()
export class RequestService extends RequestBaseService {
  public createRequest(data: any): Observable<CreateRequestResponse> {
    return this.baseCreateRequest(data);
  }

  public updateRequest(id: string, data: any): Observable<boolean> {
    return this.baseUpdateRequest(id, data);
  }

  public manageRequest(answer: string, status: number, requestId: string): Observable<boolean> {
    return this.baseManageRequest(answer, status, requestId);
  }

  public getRequestsPaging(
    pageIndex: number, pageSize: number,
    sortField: string, sortOrder: string,
    filter: Array<{ key: string; value: string[] }>,
    searchColumn: string = null,
    searchKey: string = null
  ): Observable<PageRequestResponse> {
    return this.baseGetRequest(pageIndex, pageSize, sortField, sortOrder, filter, searchColumn, searchKey);
  }

  public getRequestByRequestId(id: string): Observable<any> {
    return this.baseGetRequestByRequestId(id);
  }

  public getCommentListPaging(requestId: string, pageNo: number, pageSize: number, parentCommentId: string): Observable<any> {
    return this.baseGetComment(requestId, pageNo, pageSize, parentCommentId);
  }

  public createComment(requestId: string, replyFor: string, content: string): Observable<any> {
    return this.baseCreateComment(requestId, replyFor, content);
  }

  public getHistoryLog(requestId: string): Observable<any> {
    return this.baseGetHistoryLog(requestId);
  }
  
  public multipleChange(status: number, lst: string): Observable<UpdateResponse> {
    return this.baseMultipleChange(status, lst);
  }

  public getAllServer(): Observable<PageServerResponse>{
    return this.baseGetAllServer();
  }

  public love(requestId: string, isLove: number): Observable<any>{
    return this.baseLove(requestId, isLove);
  }
}
