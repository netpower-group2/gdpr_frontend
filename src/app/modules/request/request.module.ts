import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RequestRoutingModule } from './request-routing.module';
import { RequestManageComponent } from './manage/manage.component';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzCommentModule } from 'ng-zorro-antd/comment';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { ListRequestComponent } from './list-requests/list-request.component';
import { RequestComponent } from '../request/request.component';
import {RequestService} from './service/request.service';
import { CreateRequestComponent } from './create-request/create-request.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzCascaderModule } from 'ng-zorro-antd/cascader';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzPageHeaderModule } from 'ng-zorro-antd/page-header';

@NgModule({
  declarations: [RequestManageComponent, ListRequestComponent, RequestComponent, CreateRequestComponent],
  imports: [
    CommonModule,
    RequestRoutingModule,
    NzLayoutModule,
    NzMenuModule,
    FormsModule,
    NzCardModule,
    NzGridModule,
    NzButtonModule,
    NzDatePickerModule,
    NzListModule,
    NzCommentModule,
    NzAvatarModule,
    NzIconModule,
    NzFormModule,
    NzModalModule,
    NzSelectModule,
    NzSwitchModule,
    ReactiveFormsModule,
    NzCascaderModule,
    NzTagModule,
    NzInputModule,
    NzNotificationModule,
    NzDropDownModule,
    NzTableModule,
    NzPageHeaderModule
  ],
  providers: [RequestService]
})
export class RequestModule { }
