import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListRequestComponent } from './list-requests/list-request.component';
import { RequestManageComponent } from './manage/manage.component';
import { AuthorizationGuard } from 'src/app/_helpers/authoriztion.guard';
import {RequestComponent} from './request.component';
const routes: Routes = [
  {
    path: '',
    // redirectTo: '',
    // pathMatch: 'full',
    canActivateChild: [AuthorizationGuard],
    children: [
      {
        path: 'manage/:id',
        component: RequestManageComponent,
        data: {
          allowedRoles: ['0', '1', '2']
        }
      },
      {
        path: '',
        component: ListRequestComponent,
        data: {
          allowedRoles: ['0', '1', '2']
        }
      },
      {
        path: '**',
        redirectTo: '',
        pathMatch: 'full'
      },
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestRoutingModule { }
