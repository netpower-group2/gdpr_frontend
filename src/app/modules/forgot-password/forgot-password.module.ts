import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForgotRoutingModule } from './forgot-routing.module';
import { ForgotPasswordComponent } from './forgot-password.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { AuthenticationService } from '../../_services/authentication.service';
import { NzMessageService } from 'ng-zorro-antd/message';

@NgModule({
  declarations: [ ForgotPasswordComponent ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NzFormModule,
    NzInputModule,
    NzButtonModule,
    ForgotRoutingModule,
    FormsModule
  ],
  providers: [
    AuthenticationService,
    NzMessageService
  ]
})
export class ForgotPasswordModule { }