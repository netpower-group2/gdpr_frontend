import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { AuthenticationService } from '../../_services/authentication.service';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.less','../../../assets/css/bootstrap.min.css']
})
export class ForgotPasswordComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private authenticationService: AuthenticationService, private nzMessageService: NzMessageService) { }
  form: FormGroup;
  submitted = false;
  ngOnInit(): void {
    this.form = this.formBuilder.group({
      email: ['', [Validators.email,Validators.required]]
    });
  }

  onSubmit(value: { email: string }): void {
    this.submitted = true;
    for (const key in this.form.controls) {
      this.form.controls[key].markAsDirty();
      this.form.controls[key].updateValueAndValidity();
    }
    this.authenticationService.forgotPassword(value.email).subscribe(res => {
      if (res.errors) {
        this.nzMessageService.error("Invalid request!");
      }
      else {
        this.nzMessageService.info("Please check your email for a new password!");
        this.form.reset();
      }
    });
  }

}
