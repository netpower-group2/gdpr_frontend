import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServerComponent } from './server.component';
import { RequestManageComponent } from '../request/manage/manage.component';

import { AuthorizationGuard } from 'src/app/_helpers/authoriztion.guard';
import { ListServersComponent } from './list-servers/list-servers.component';
const routes: Routes = [
  {
    path: '', component: ListServersComponent,
    canActivateChild: [AuthorizationGuard],
    children: [
      {
        path: 'manage',
        component: RequestManageComponent,
        data: {
          allowedRoles: ['0', '2']
        }
      },
      {
        path: '**',
        component: ServerComponent,
        data: {
          allowedRoles: ['0', '2']
        }

      },
      // {
      //   path: '**',
      //   redirectTo: '',
      //   pathMatch: 'full'
      // },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServerRoutingModule { }
