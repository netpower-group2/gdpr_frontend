import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { User } from 'src/app/_models/user.model';
import { UserService } from '../../user/service/user.service';

@Component({
  selector: 'app-export-request',
  templateUrl: './export-request.component.html',
  styleUrls: ['./export-request.component.less','../../../../assets/css/bootstrap.min.css']
})
export class ExportRequestComponent implements OnInit {

  isVisible = false;
  dataSource: User[] = [];
  listRequester = [];
  listApprover = [];
  @Output() listDataExport = new EventEmitter<any>();
  @Input() showProcessData: boolean;
  showOrHiddenFormExport() {
    this.isVisible = !this.isVisible;
    this.loadDataUser(1,200,[]);
  }
  startValue: Date | null = null;
  endValue: Date | null = null;
  endOpen = false;

  disabledStartDate = (startValue: Date): boolean => {
    if (!startValue || !this.endValue) {
      return false;
    }
    return startValue.getTime() > this.endValue.getTime();
  };

  disabledEndDate = (endValue: Date): boolean => {
    if (!endValue || !this.startValue) {
      return false;
    }
    return endValue.getTime() <= this.startValue.getTime();
  };

  onStartChange(date: Date): void {
    this.startValue = date;
  }

  onEndChange(date: Date): void {
    this.endValue = date;
  }
  
  validateForm: FormGroup;
  constructor(private fb: FormBuilder,
              private userService: UserService ) {
    this.validateForm = this.fb.group({
      fromDate: ['',[]],
      toDate: ['', []],
      requesterName: ['',[]],
      approverName: ['',[]],
    });
   }

  ngOnInit(): void {
  }


  submitForm(value: { fromDate: Date; toDate: Date ,requesterName : string, approverName: string}): void {
    for (const key in this.validateForm.controls) {
      this.validateForm.controls[key].markAsDirty();
      this.validateForm.controls[key].updateValueAndValidity();
    }
    this.listDataExport.emit(value);
  }
  loadDataUser(
    pageIndex: number,
    pageSize: number,
    filter: Array<{ key: string; value: string[] }>
  ): void {
    this.userService.getUsers(pageIndex, pageSize, null, null, filter,null).subscribe(data => {
      if (data.success) {
        this.dataSource = data.userDetails.map(u => new User(
          u.id,
          u.createdDate,
          u.firstName,
          u.lastName,
          u.email,
          u.role,
          u.status,
          u.birthday,
          u.isDeleted,
          u.avatar)
        );
        this.listRequester = this.dataSource;
        this.listApprover = this.listRequester.filter(requester => requester.role == 0)
      }
    });
  }

}
