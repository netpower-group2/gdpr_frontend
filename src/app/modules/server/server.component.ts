import { Component, OnInit } from '@angular/core';
import {Server} from '../../_models/server.model';
import {Router} from '@angular/router';
import { ServerService } from './service/server.service';
@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.less']
})
export class ServerComponent implements OnInit {
  constructor(  private router: Router, private serverService: ServerService) {}
  ngOnInit(): void {
  }
 }
