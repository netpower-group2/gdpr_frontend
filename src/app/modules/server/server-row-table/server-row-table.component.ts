import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'tr[app-server-row-table]',
  templateUrl: './server-row-table.component.html',
  styleUrls: ['./server-row-table.component.less']
})
export class ServerRowTableComponent implements OnInit {
  @Input() actor: any;
  @Input() listTitle: any[];
  @Input() nzcheck: boolean;

  @Output() checkevt = new EventEmitter();
  @Output() onEditAction: EventEmitter<any> = new EventEmitter();
  @Output() onViewManage: EventEmitter<any> = new EventEmitter();
 // @Output() onDeleteAction: EventEmitter<string> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }
  onEditItem() {
    this.onEditAction.emit(this.actor);
  }
  onViewEvt(){
    this.onViewManage.emit(this.actor);
  }
}
