import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServerRowTableComponent } from './server-row-table.component';

describe('ServerRowTableComponent', () => {
  let component: ServerRowTableComponent;
  let fixture: ComponentFixture<ServerRowTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServerRowTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServerRowTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
