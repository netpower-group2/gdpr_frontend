import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServerService } from '../service/server.service';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { Server } from 'src/app/_models/server.model';
import { NzMessageService } from 'ng-zorro-antd/message';
import { saveAs } from 'file-saver';
import * as moment from 'moment';
@Component({
  selector: 'app-list-servers',
  templateUrl: './list-servers.component.html',
  styleUrls: ['./list-servers.component.less']
})
export class ListServersComponent implements OnInit {
  visibleServer = false;
  visibleIpAddress = false;
  visibleOwner = false;
  searchServerValue = '';
  searchIpAddressValue = '';
  searchOwnerValue = '';
  checked = false;
  isLoading = false;
  indeterminate = false;
  setOfCheckedId = new Set<string>();
  dataSource: Server[] = [];
  listOfDisplayData: Server[] = [];
  total = 10000000;
  processData = false;
  index: number = 1;
  filterStatus = [
    { text: 'Active', value: '1' },
    { text: 'In-active', value: '0' }
  ]

  filterFn = (status: number, item: Server) => item.status == status;
  onResetCheckList() {
    this.setOfCheckedId.clear();
    this.checked = false;
    this.indeterminate = false;
  }
  constructor(private serverService: ServerService,
    private router: Router,
    private nzMessageService: NzMessageService) { }
  sortNameServer = (a: Server, b: Server) => a.nameServer.localeCompare(b.nameServer);
  sortIpAddress = (a: Server, b: Server) => a.ipAddress.localeCompare(b.ipAddress);
  sortOwner = (a: Server, b: Server) => a.companyName.localeCompare(b.companyName);

  normalizeDate(date: string) {
    return moment(date).format('YYYY.MM.DD');
  }
  
  ngOnInit(): void {
    this.loadDataServer();

  }

  addNewServer(event: boolean): void {
    if (event === true) {
      this.loadDataServer();
    }
  }

  editServer(event: Server): void {
    if (event != null) {
      
    }
  }


  onCurrentPageDataChange(listOfCurrentPageData: Server[]): void {
    this.listOfDisplayData = listOfCurrentPageData;
    this.setOfCheckedId.clear();
    this.refreshCheckedStatus();
  }

  updateCheckedSet(id: string, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
    var arrId = Array.from(this.setOfCheckedId).length;
    if (arrId != 0) {
      this.processData = true;
    }
    else {
      this.processData = false;
    }
  }

  onItemChecked(id: string, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  onAllChecked(value: boolean): void {
    this.listOfDisplayData.forEach(item => this.updateCheckedSet(item.serverId, value));
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    this.checked = this.listOfDisplayData.every(item => this.setOfCheckedId.has(item.serverId));
    this.indeterminate = this.listOfDisplayData.some(item => this.setOfCheckedId.has(item.serverId)) && !this.checked;
  }

  loadDataServer(): void {
    this.index = 1;
    this.isLoading = true;
    this.serverService.getAllServer().subscribe(data => {
      this.isLoading = false;
      if (data.success) {
        this.dataSource = data.serverDetails.map(s => new Server(
          this.index++,
          s.serverId,
          s.createdDate,
          s.createdBy,
          s.updatedDate,
          s.updatedBy,
          s.startDate ? this.normalizeDate(s.startDate) : null,
          s.endDate ? this.normalizeDate(s.endDate) : null,
          s.nameServer,
          s.ipAddress,
          s.customerId,
          s.status,
          s.companyName
        )
        );
        // this.listOfDisplayData = [...this.dataSource];
      }
    });
  }

  reset(): void {
    if (this.searchServerValue !== '') {
      this.searchServerValue = '';
    }
    if (this.searchIpAddressValue !== '') {
      this.searchIpAddressValue = '';
    }
    if (this.searchOwnerValue !== '') {
      this.searchOwnerValue = '';
    }
    this.loadDataServer();
    // this.listOfDisplayData = this.dataSource;

    this.refreshCheckedStatus();
  }

  searchServer(): void {
    this.visibleServer = false;
    this.dataSource = this.dataSource.filter((item: Server) => item.nameServer.indexOf(this.searchServerValue) !== -1);
    this.setOfCheckedId.clear();
    this.refreshCheckedStatus();
  }

  searchIpAddress(): void {
    this.visibleIpAddress = false;
    this.dataSource = this.dataSource.filter((item: Server) => item.ipAddress.indexOf(this.searchIpAddressValue) !== -1);
    this.setOfCheckedId.clear();
    this.refreshCheckedStatus();
  }

  searchOwner(): void {
    this.visibleOwner = false;
    this.dataSource = this.dataSource.filter((item: Server) => (item.companyName != null) ? item.companyName.indexOf(this.searchOwnerValue) !== -1 : null);
    this.setOfCheckedId.clear();
    this.refreshCheckedStatus();
  }

  multipleAction(status: number): void {
    var arrId = Array.from(this.setOfCheckedId);
    var lstId = arrId.join(',');

    this.serverService.multipleChange(status, lstId).subscribe(res => {
      if (res.success) {
        arrId.forEach(element => {
          this.listOfDisplayData[this.listOfDisplayData.findIndex(s => s.serverId == element)].status = status;
        });
        this.nzMessageService.success('Updated Successful!')
      }
      else { this.nzMessageService.error('Something went wrong!'); }
    });
  }

  getListDataExport(event: any): void {
    if (event != null) {
      const arrId = Array.from(this.setOfCheckedId);
      const lstId = arrId.join(',');
      this.serverService.exportServer(lstId, event.fromDate,
        event.toDate,
        event.requesterName,
        event.approverName)
        .subscribe(
          blob => {
            if (blob) {
              saveAs(blob, 'RequestsByServer.csv');
            }
            else {
            }
          }
        );
    }

  }
}
