import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RequestModel } from '../../../_models/request.model';

interface ItemData {
  id: number;
}

@Component({
  selector: 'app-server-table',
  templateUrl: './server-table.component.html',
  styleUrls: ['./server-table.component.less']
})
export class ServerTableComponent implements OnInit {
  @Input() pageIndex: number;
  @Input() pageSize: number;
  @Input() listTitle: any[];
  @Input() data: any[];
  dataShow: any[];

  @Input() totalPage: number;
  @Output() onChangePageIndex = new EventEmitter<number>();
  @Output() onChangePageSize = new EventEmitter<number>();
  @Output() onEditObject: EventEmitter<any> = new EventEmitter();
  @Output() onViewObject: EventEmitter<any> = new EventEmitter();

  checked = false;
  indeterminate = false;
  listOfCurrentPageData: ItemData[] = [];
  setOfCheckedId = new Set<number>();
  constructor() { }
  // ngOnChange(){
  //   this.dataShow = this.data;
  // }
  updateCheckedSet(id: number, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  onItemChecked(id: number, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  onAllChecked(value: boolean): void {
    this.listOfCurrentPageData.forEach(item => this.updateCheckedSet(item.id, value));
    this.refreshCheckedStatus();
  }

  onCurrentPageDataChange($event: ItemData[]): void {
    this.listOfCurrentPageData = $event;
    this.refreshCheckedStatus();
    this.setOfCheckedId.clear();
  }
  onCurrentPageIndexChange($event: number): void {
    this.pageIndex = $event;
    this.onChangePageIndex.emit(this.pageIndex);
  }

  onCurrentPageSizeChange($event: number): void {
    this.pageSize = $event;
    this.onChangePageSize.emit(this.pageSize);
  }
  refreshCheckedStatus(): void {
    this.checked = this.listOfCurrentPageData.every(item => this.setOfCheckedId.has(item.id));
    this.indeterminate = this.listOfCurrentPageData.some(item => this.setOfCheckedId.has(item.id)) && !this.checked;
  }

  onEditAction(evt: any): void {
    this.onEditObject.emit(evt);
  }

  onViewAction(evt: any): void {
    this.onViewObject.emit(evt);
  }

  isRequest(): boolean {
    if (typeof (this.data[0]) === typeof (RequestModel)) {
      return true;
    }
    else { return false; }
  }

  ngOnInit(): void {
    this.dataShow = this.data;
  }
}
