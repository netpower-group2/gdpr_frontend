import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {ServerBaseService} from '../../../_services/server.service';
import {Server} from '../../../_models/server.model';
import { PageServerResponse } from 'src/app/_models/pageserver-response';
import { CreateServerResponse } from 'src/app/_models/createserver-response';
import { UpdateResponse } from 'src/app/_models/update-response';

@Injectable()
export class ServerService extends ServerBaseService{
  public createServer(nameServer: string, ipAddress: string, customerId: string, status: number, startDate : string, endDate: string ): Observable<CreateServerResponse> {
    return this.baseCreateServer(nameServer,ipAddress,customerId,status,startDate,endDate);
  }

  public updateServer(serverId:string,nameServer:string ,ipAddress:string,customerId:string,status: number,startDate : string, endDate: string ): Observable<UpdateResponse> {
    return this.baseUpdateServer(serverId, nameServer,ipAddress,customerId,status,startDate,endDate);
  }

  public getAllServer(): Observable<PageServerResponse> {
    return this.baseGetAllServer();
  }

  public getServerById(id: string): Observable<Server> {
    return this.baseGetServerById(id);
  }

  public deleteServer(id: string): Observable<boolean> {
    return this.baseDeleteServer(id);
  }
  public multipleChange(status: number, lst: string): Observable<UpdateResponse> {
    return this.baseMultipleChange(status, lst);
  }
  public exportServer(listID: string,fromDate: string,toDate:string, requester:string,approver:string ): Observable<Blob> {
    return this.baseExportServer(listID,fromDate,toDate,requester,approver);
  }

}
