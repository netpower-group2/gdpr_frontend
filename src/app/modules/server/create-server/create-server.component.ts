import { Component, OnInit,Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Server } from '../../../_models/server.model';
import { ServerService } from '../service/server.service';
import * as moment from 'moment';
import { NzMessageService } from 'ng-zorro-antd/message';
@Component({
  selector: 'app-create-server',
  templateUrl: './create-server.component.html',
  styleUrls: ['./create-server.component.less']
})
export class CreateServerComponent implements OnInit {
  @Input() actionName: string;
  @Input() data: Server;
  @Output() createServer = new EventEmitter<boolean>();
  @Output() editServer = new EventEmitter<Server>();
  actionDisplay: string;
  isVisible = false;
  switchValue = false;
  isLoading = false;
  validateForm: FormGroup;
  showModal(): void {
    this.isVisible = true;
  }
  normalizeDate(date: string) {
    return moment(date).format('YYYY.MM.DD')
  }
  handleCancel(): void {
    this.isVisible = false;
  }
  constructor(private fb: FormBuilder, 
              private serverService: ServerService,
              private nzMessageService: NzMessageService) {
   }

  ngOnInit(): void {
    this.actionDisplay = (this.actionName == "create") ? "Create New Server" : "Edit";
    if (this.actionName == "create"){
      this.validateForm = this.fb.group({
        serverName: ['',[Validators.required]],
        ipAddress: ['', [Validators.required]],
        serverStartDate: ['', [Validators.required]],
        serverEndDate: ['', [ Validators.required]],
      });
    }
    else {
      this.validateForm = this.fb.group({
        serverName: [this.data.nameServer,[Validators.required]],
        ipAddress: [this.data.ipAddress, [Validators.required]],
        serverStartDate: [this.data.startDate, []],
        serverEndDate: [this.data.endDate, []],
        statusServer: [this.data.status, []]
      });
    }
  }
  startValue: Date | null = null;
  endValue: Date | null = null;
  disabledStartDate = (startValue: Date): boolean => {
    if (!startValue || !this.endValue) {
      return false;
    }
    return startValue.getTime() > this.endValue.getTime();
  };

  disabledEndDate = (endValue: Date): boolean => {
    if (!endValue || !this.startValue) {
      return false;
    }
    return endValue.getTime() <= this.startValue.getTime();
  };
  onStartChange(date: Date): void {
    this.startValue = date;
  }

  onEndChange(date: Date): void {
    this.endValue = date;
  }

  submitForm(value: { serverName: string; ipAddress: string; serverStartDate: string; serverEndDate: string; statusServer: number }): void {
    for (const key in this.validateForm.controls) {
      this.validateForm.controls[key].markAsDirty();
      this.validateForm.controls[key].updateValueAndValidity();
    }
    
    if (this.actionName == "create") {
      this.serverService.createServer(value.serverName, value.ipAddress,null,1,value.serverStartDate,value.serverEndDate).subscribe(res => {
        if (res.success) {
          this.createServer.emit(true);
          this.nzMessageService.success("Create Successful!")
        }
        else {
          this.createServer.emit(null);
          this.nzMessageService.error("Something went wrong!");
        }
        this.isLoading = false;
        this.isVisible = false;
        this.validateForm.reset();
      });
    } 
    else {
      this.data.nameServer = value.serverName;
      this.data.ipAddress = value.ipAddress;
      this.data.startDate = value.serverStartDate?this.normalizeDate(value.serverStartDate):null;
      this.data.endDate = value.serverEndDate?this.normalizeDate(value.serverEndDate):null;
      this.data.status = value.statusServer?1:0;
      
      this.serverService.updateServer(this.data.serverId,this.data.nameServer,this.data.ipAddress,null,this.data.status,this.data.startDate,this.data.endDate).subscribe(res => {
        if (res.success) {
          this.editServer.emit(this.data);
          this.nzMessageService.success("Updated Successful!")
        }
        else {
          this.nzMessageService.error("Something went wrong!");
          this.editServer.emit(null);
        }
          this.isLoading = false;
          this.isVisible = false;
      });
    }
  }
}
