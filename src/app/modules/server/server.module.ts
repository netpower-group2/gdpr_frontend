import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServerRoutingModule } from './server-routing.module';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzCommentModule } from 'ng-zorro-antd/comment';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { ServerComponent } from './server.component';
import {ServerService} from './service/server.service';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { CreateServerComponent } from './create-server/create-server.component';
import { ExportRequestComponent } from './export-request/export-request.component';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { ListServersComponent } from './list-servers/list-servers.component';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzMessageService } from 'ng-zorro-antd/message';
import { UserService } from '../user/service/user.service';
@NgModule({
  declarations: [ServerComponent, CreateServerComponent, ExportRequestComponent, ListServersComponent],
  imports: [
    CommonModule,
    ServerRoutingModule,
    NzLayoutModule,
    NzMenuModule,
    FormsModule,
    NzCardModule,
    NzGridModule,
    NzButtonModule,
    NzDatePickerModule,
    NzListModule,
    NzCommentModule,
    NzAvatarModule,
    NzFormModule,
    NzInputModule,
    NzTabsModule,
    NzCheckboxModule,
    NzSelectModule,
    NzSwitchModule,
    ReactiveFormsModule,
    NzModalModule,
    NzIconModule,
    NzTableModule,
    NzTagModule,
    NzDropDownModule
  ],
  providers: [
    ServerService,
    NzMessageService,
    UserService
  ]
})
export class ServerModule { }
