import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {
  isCollapsed = true;
  rol: number;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private jwtHelperService: JwtHelperService
  ) { }

  ngOnInit(): void {
    if (localStorage.getItem('userRole') == null) {
      this.router.navigate(['/login']);
    }
    else {
      this.rol = this.jwtHelperService.decodeToken(JSON.parse(localStorage.getItem('userRole')).authToken).rol;
    }
  }

  logOut(): void {
    this.router.navigate(['/login']);
    localStorage.removeItem('userRole');
  }
}
