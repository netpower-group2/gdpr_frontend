import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HomeBaseService } from '../../../_services/homebase.service';

@Injectable()
export class HomeService extends HomeBaseService {

  public getRequestStatistic(): Observable<any> {
    return this.baseGetRequestStatistic();
  }

}
