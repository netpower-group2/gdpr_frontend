import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home-routing.module';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzPageHeaderModule } from 'ng-zorro-antd/page-header';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzDescriptionsModule } from 'ng-zorro-antd/descriptions';
import { StatisticComponent } from './statistic/statistic.component';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { HomeService } from './service/home.service';
import { RequestService } from '../request/service/request.service';
@NgModule({
  declarations: [HomeComponent, StatisticComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    NzButtonModule,
    NzMenuModule,
    NzIconModule,
    NzLayoutModule,
    NzDropDownModule,
    NzPageHeaderModule,
    NzAvatarModule,
    NzDescriptionsModule,
    NzCollapseModule,
    NzBadgeModule
  ],
  providers: [HomeService, RequestService]
})
export class HomeModule { }
