import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HomeService } from '../service/home.service';
import { RequestService } from '../../request/service/request.service';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-statistic',
  templateUrl: './statistic.component.html',
  styleUrls: ['./statistic.component.less']
})
export class StatisticComponent implements OnInit {
  rol: number;
  numOfNew = 0;
  numOfApproved = 0;
  numOfClosed = 0;
  listIdObsoletedString: string;
  listIdObsoleted = [];
  listTitleObsoletedString: string;
  listTitleObsoleted = [];
  listObsoleted: any = [];


  constructor(
    private homeService: HomeService,
    private requestService: RequestService,
    private jwtHelperService: JwtHelperService
  ) { }

  ngOnInit(): void {
    this.rol = this.jwtHelperService.decodeToken(JSON.parse(localStorage.getItem('userRole')).authToken).rol;
    this.getStatistic();
  }

  getStatistic(): void {
    this.homeService.getRequestStatistic().subscribe(res => {
      this.numOfNew = res.requestStatisticDetail.numOfNew;
      this.numOfApproved = res.requestStatisticDetail.numOfApproved;
      this.numOfClosed = res.requestStatisticDetail.numOfClosed;
      this.listIdObsoletedString = res.requestStatisticDetail.listObsoleted;
      this.listIdObsoleted = this.listIdObsoletedString.split(',');
      this.listTitleObsoletedString = res.requestStatisticDetail.listTitleObsoleted;
      this.listTitleObsoleted = this.listTitleObsoletedString.split(',');
      if (this.listIdObsoletedString.length !== 0){
        this.listObsoleted = this.listIdObsoleted.map((e, i) => [e, this.listTitleObsoleted[i]]);
      }
    });
  }

  closeAllFromHome(): void{
    this.requestService.multipleChange(4, this.listIdObsoletedString).subscribe(res => {
      this.getStatistic();
      this.listObsoleted = [];
    });
  }

}
