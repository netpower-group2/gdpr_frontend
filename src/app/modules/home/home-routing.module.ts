import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthorizationGuard } from '../../_helpers/authoriztion.guard';
import { HomeComponent } from './home.component';
import { CustomerComponent } from '../customer/customer.component';
import { ServerComponent } from '../server/server.component';
import { AccountComponent } from '../account/account.component';
import { StatisticComponent } from './statistic/statistic.component';

const routes: Routes = [

    {
        path: '', component: HomeComponent,
        canActivate: [AuthorizationGuard],
        canActivateChild: [AuthorizationGuard],
        children: [
            {
                path: 'customer',
                loadChildren: () => import('../customer/customer.module').then(m => m.CustomerModule),
                data: {
                  allowedRoles: ['0', '3']
                }
              },
              {
                path: 'server',
                loadChildren: () => import('../server/server.module').then(m => m.ServerModule),
                data: {
                  allowedRoles: ['0', '2']
                }
              },
              {
                path: 'request',
                loadChildren: () => import('../request/request.module').then(m => m.RequestModule),
                data: {
                  allowedRoles: ['0', '1', '2']
                }
              },
              {
                path: 'user',
                loadChildren: () => import('../user/user.module').then(m => m.UserModule),
                data: {
                  allowedRoles: ['0']
                }
              },
              {
                path: 'account',
                loadChildren: () => import('../account/account.module').then(m => m.AccountModule),
                data: {}
              },
            {
                path: 'home',
                component: StatisticComponent,
                data: {}
            },
            {
                path: '',
                redirectTo: 'home',
                pathMatch: 'full'
            },
        ]
    }

];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HomeRoutingModule { }
