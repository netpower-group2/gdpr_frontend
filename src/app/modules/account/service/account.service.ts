import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UpdateResponse } from 'src/app/_models/update-response';
import { UserBaseService } from '../../../_services/user.service'

@Injectable()
export class AccountService extends UserBaseService {
    // public uploadAvatar(email: string, role: number): Observable<CreateUserResponse> {
    //   return this.baseCreateUser(email, role);
    // }
    public getInfo() {
        return this.baseGetInfoUser();
    }

    public uploadAvatar(url: string): Observable<UpdateResponse> {
        return this.baseUploadAvatar(url);
    }

    public updateInfo(firstName: string, lastName: string, birthday: Date): Observable<UpdateResponse> {
        return this.baseUpdateInfo(firstName, lastName, birthday);
    }

    public changePass(oldPass: string, newPass: string): Observable<UpdateResponse> {
        return this.baseChangePass(oldPass, newPass);
    }
}