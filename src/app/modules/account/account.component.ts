import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Observable, Observer } from 'rxjs';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { Ng2ImgMaxService } from 'ng2-img-max';
import { NzMessageService } from 'ng-zorro-antd/message';
import { User } from 'src/app/_models/user.model';
import { AccountService } from './service/account.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.less', '../../../assets/css/bootstrap.min.css'],

})
export class AccountComponent implements OnInit {
  currentUser: User = new User(null, null, null, null, null, null, null, null, false, null);
  croppedImage: string = null;
  imageChangedEvent: any = null;
  isShowFormAccount = true;
  validateFormChangeInfo: FormGroup;
  selectedImage: any = null;
  isSubmitted: boolean;
  uploadImage: string;

  constructor(private fb: FormBuilder, private ng2ImgMaxService: Ng2ImgMaxService, 
              private accountService: AccountService, private nzMessageService: NzMessageService) {
    this.validateFormChangeInfo = this.fb.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      birthday: ['', [Validators.required]],
    });
    this.validateFormChangePw = this.fb.group({
      currentPassword: ['', [Validators.required]],
      newPassword: ['', [Validators.required]],
      confirmPassword: ['', [this.confirmValidator]],
    });
  }
  
  ngOnInit(): void {
    this.getCurrentUserInfo();
  }

  getCurrentUserInfo(): void {
    this.accountService.getInfo().subscribe(res => {
      if (res.success) {
        this.currentUser = new User(res.userInfo.id,
          res.userInfo.createdDate,
          res.userInfo.firstName,
          res.userInfo.lastName,
          res.userInfo.email,
          res.userInfo.role,
          res.userInfo.status,
          res.userInfo.birthday,
          res.userInfo.isDeleted,
          res.userInfo.avatar);
        this.croppedImage = this.currentUser.avatar;
        this.validateFormChangeInfo.controls['firstName'].setValue(this.currentUser.firstName);
        this.validateFormChangeInfo.controls['lastName'].setValue(this.currentUser.lastName);
        this.validateFormChangeInfo.controls['birthday'].setValue(this.currentUser.birthday);
      }
    });
  }

  fileChangeEvent(event: any): void {
    const { target } = event;
    if (target.value.length > 0) {
      var ext = target.value.split('.').pop().toLowerCase();
      if (ext == 'jpg' || ext == 'png') {
        this.imageChangedEvent = event;
      }
      else {
        target.value = "";
        this.imageChangedEvent = null;
        this.nzMessageService.warning("Please only upload png/jpg image file!");
        this.croppedImage = this.currentUser.avatar;
      }
    } 
    else {
      target.value = "";
      this.imageChangedEvent = null;
      this.croppedImage = this.currentUser.avatar;
    }
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }

  async imageResize() {
    var file = await fetch(this.croppedImage).then(res => res.arrayBuffer())
      .then(buf => new File([buf], "temp", { type: "image/png" }));
    this.ng2ImgMaxService.resizeImage(file, 200, 200).subscribe(res => {
      const reader = new FileReader();
      reader.readAsDataURL(res);
      reader.onload = () => {
        this.uploadImage = reader.result.toString();
        this.imageUpload();
      };
    })
  }

  async onSubmit() {
    this.imageResize();
  }

  imageUpload() {
    this.accountService.uploadAvatar(this.uploadImage).subscribe(res => {
      if (res.success) {
        this.imageChangedEvent = null;
        this.croppedImage = this.uploadImage;
        this.nzMessageService.success("Upload successful!");
      }
      else {
      }
    });
  }

  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }

  cancelUpload() {
    this.imageChangedEvent = null;
    this.croppedImage = this.currentUser.avatar;
  }


  showFormAccount() {
    this.isShowFormAccount = true;
  }

  showFormPassword() {
    this.isShowFormAccount = false;
  }

  submitFormChangeInfo(value: { firstName: string; lastName: string; birthday: Date }): void {
    for (const key in this.validateFormChangeInfo.controls) {
      this.validateFormChangeInfo.controls[key].markAsDirty();
      this.validateFormChangeInfo.controls[key].updateValueAndValidity();
    }
    this.accountService.updateInfo(value.firstName, value.lastName, value.birthday).subscribe(res => {
      if (res.success) {
        this.nzMessageService.success("Change info successful!");
        this.getCurrentUserInfo();
        this.validateFormChangeInfo.reset();
      }
    },
    error => {
      this.nzMessageService.error("Cannot change your info.");
    });
  }

  resetForm(e: MouseEvent): void {
    e.preventDefault();
    this.validateFormChangeInfo.reset();
    for (const key in this.validateFormChangeInfo.controls) {
      this.validateFormChangeInfo.controls[key].markAsPristine();
      this.validateFormChangeInfo.controls[key].updateValueAndValidity();
    }
  }



  // Change Password
  validateFormChangePw: FormGroup;
  validateConfirmPassword(): void {
    // setTimeout(() => this.validateForm.controls.confirm.updateValueAndValidity());
  }

  confirmValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { error: true, required: true };
    } else if (control.value !== this.validateFormChangePw.controls.newPassword.value) {
      return { confirm: true, error: true };
    }
    return {};
  };

  submitFormChangePw(value: { currentPassword: string; newPassword: string; confirmPassword: string }): void {
    for (const key in this.validateFormChangePw.controls) {
      this.validateFormChangePw.controls[key].markAsDirty();
      this.validateFormChangePw.controls[key].updateValueAndValidity();
    }
    this.accountService.changePass(value.currentPassword, value.newPassword).subscribe(res => {
      if (res.success) {
        this.nzMessageService.success("Your password is changed!");
        this.validateFormChangePw.reset();
      }
    },
    error => {
      this.nzMessageService.error("Wrong password!");
    });
  }
  // End Change Password

  //Change avatar 


  // showPreview(event: any) {
  //   if (event.target.files && event.target.files[0]) {
  //     const reader = new FileReader();
  //     reader.onload = (e: any) => this.imgSrc = e.target.result;
  //     reader.readAsDataURL(event.target.files[0]);
  //     this.selectedImage = event.target.files[0];
  //     this.selectedImageName = this.selectedImage.name;
  //   }
  //   else {
  //     this.imgSrc = '../../../assets/img/default_avatar.png';
  //     this.selectedImage = null;
  //     this.selectedImageName = "";
  //   }
  // }
  formChangeAvatar = new FormGroup({
    imageUrl: new FormControl('', Validators.required)
  })

  get formControls() {
    return this.formChangeAvatar['controls'];
  }

  //End Change avatar




}
