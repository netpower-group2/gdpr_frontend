import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search-customer',
  templateUrl: './search-customer.component.html',
  styleUrls: ['./search-customer.component.less']
})
export class SearchCustomerComponent implements OnInit {
  @Input() searchValue: string;
  @Output() searchAction = new EventEmitter<string>();
  searchView: string;
  inputValue?: string;
  options: string[] = [];

  onInput(event: Event): void {
    const value = (event.target as HTMLInputElement).value;
    this.options = value ? [value, value + value, value + value + value] : [];
  }
  onSearch(): void{
    this.searchAction.emit(this.searchView);
  }
  changeSearchValue(evt: string): void{
    this.searchView = evt;
  }
  ngOnInit(): void{
    this.searchView = this.searchValue;
  }

}
