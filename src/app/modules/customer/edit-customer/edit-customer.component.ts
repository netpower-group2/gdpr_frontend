import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import {Customer} from '../../../_models/customer.model';
import {CustomerService} from '../service/customer.service';
import {FilterItem} from '../../../_models/filteritem.model';
@Component({
  selector: 'app-edit-customer',
  templateUrl: './edit-customer.component.html',
  styleUrls: ['./edit-customer.component.less']
})
export class EditCustomerComponent implements OnChanges {

  @Input() custom: Customer;
  @Input() cpFilterList: FilterItem[];
  @Output() ChangeCustomer = new EventEmitter<boolean>();
  validateForm: FormGroup;

  isVisible: boolean;

  handleCancel(): void {
    this.isVisible = false;

  }
  constructor(private fb: FormBuilder, public customerService: CustomerService) {
    this.validateForm = this.fb.group({
      name: ['', [Validators.required]],
      contractBeginDate: ['', [Validators.required]],
      contractEndDate: ['', [Validators.required]],
      contactPoint: ['', [ Validators.required]],
      description: [''],
      status: [],
    });
   }

  ngOnInit(): void {
  }

  changeStatus(evt: boolean): void{
    if (evt === true){
      this.validateForm.value.status = 1;
    }
    else{
       this.validateForm.value.status = 0;
    }
  }

  ngOnChanges(): void{

    this.validateForm = this.fb.group({
      name: [this.custom.name, [Validators.required]],
      contractBeginDate: [this.custom.contractBeginDate , [Validators.required]],
      contractEndDate: [this.custom.contractEndDate , [Validators.required]],
      contactPoint: [this.custom.contactPointId, [ Validators.required]],
      description: [this.custom.description],
      status: [this.custom.status],
    });

  }
  showModal(): void{
    this.isVisible = true;
  }

  startValue: Date | null = null;
  endValue: Date | null = null;
  endOpen = false;

  disabledStartDate = (startValue: Date): boolean => {
    if (!startValue || !this.endValue) {
      return false;
    }
    return startValue.getTime() > this.endValue.getTime();
  };

  disabledEndDate = (endValue: Date): boolean => {
    if (!endValue || !this.startValue) {
      return false;
    }
    return endValue.getTime() <= this.startValue.getTime();
  };

  onStartChange(date: Date): void {
    this.startValue = date;
  }

  onEndChange(date: Date): void {
    this.endValue = date;
  }

  submitForm(value: { name: string,
            contractBeginDate: Date
            contractEndDate: Date,
            contactPoint: string,
            status: boolean,
            description: string}): void
  {
    // for (const key in this.validateForm.controls) {
    //   this.validateForm.controls[key].markAsDirty();
    //   this.validateForm.controls[key].updateValueAndValidity();
    // }
    this.customerService.updateCustomer(this.custom.id, value)
    .subscribe(res => {
        this.ChangeCustomer.emit(true);
    });

  }


}
