import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CustomerBaseService } from '../../../_services/basecustomer.service';

import { Customer } from '../../../_models/customer.model';
import { PageCustomerResponse } from '../../../_models/pagecustomer-response';
import { CreateCustomerResponse } from '../../../_models/createcustomer-response';
import { CPResponse } from '../../../_models/cp-response';
import {PageServerResponse} from '../../../_models/pageserver-response';
import {BaseUpdateResponse} from '../../../_models/baseupdate-response';
@Injectable()
export class CustomerService extends CustomerBaseService {
  base: CustomerBaseService;
  public createCustomer(data: { name: string,
                                contractBeginDate: Date,
                                contractEndDate: Date,
                                contactPoint: string,
                                status: boolean,
                                description: string}): Observable<CreateCustomerResponse> {
    return this.baseCreateCustomer(data);
  }

  public updateCustomer(id: string, value: {  name: string,
                                              contractBeginDate: Date,
                                              contractEndDate: Date,
                                              contactPoint: string,
                                              status: boolean,
                                              description: string}): Observable<boolean> {
    return this.baseUpdateCustomer(id, value);
  }

  public getAllCustomer(): Observable<PageCustomerResponse> {
    return this.baseGetAllCustomer();
  }

  public getCP(): Observable<CPResponse> {
    return this.baseGetCP();
  }

  public exportRequest(id: string, fromDate: Date, toDate: Date): Observable<Blob>{
    return this.baseExportRequestByCustomer(id, fromDate, toDate);
  }

  public getServer(): Observable<PageServerResponse> {
    return this.baseGetServer();
  }

  public updateCustomerServer(customerId: string, listServerAddOwn: string, listServerUnOwn: string): Observable<BaseUpdateResponse> {
    // tslint:disable-next-line: max-line-length
    return this.baseUpdateCustomerServer(customerId, listServerAddOwn, listServerUnOwn);
  }

  public updateMultiActionCustomer(status: number, idList: string): Observable<BaseUpdateResponse> {
    // tslint:disable-next-line: max-line-length
    return this.baseMultiActionCustomer(status, idList);
  }

  public getPageCustomer( searchPageKey: string, searchColumn: string, searchColumnKey: string,
                          pageNo: number, pageSize: number, sortColumn: string, sortType: string,
                          filterColumn: string, filterValue: string): Observable<PageCustomerResponse> {
    // tslint:disable-next-line: max-line-length
    return this.baseGetPageCustomer(searchPageKey, searchColumn, searchColumnKey, pageNo, pageSize, sortColumn, sortType, filterColumn, filterValue);
  }
  public getCustomerById(id: string): Observable<Customer> {
    return this.baseGetCustomerById(id);
  }

  public deleteCustomer(id: string): Observable<boolean> {
    return this.baseDeleteCustomer(id);
  }
}
