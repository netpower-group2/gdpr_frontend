import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
@Component({
  selector: 'app-export-customer',
  templateUrl: './export-customer.component.html',
  styleUrls: ['./export-customer.component.less','../../../../assets/css/bootstrap.min.css']
})
export class ExportCustomerComponent implements OnInit {
  isVisible = false;
  @Output() Export = new EventEmitter<Date[]>();
  showOrHiddenFormExport() {
    this.isVisible = !this.isVisible;
  }
  validateForm: FormGroup;
  // handleCancel(): void {
  //   this.isVisible = false;
  // }
  constructor(private fb: FormBuilder) {
    this.validateForm = this.fb.group({
      fromDate: [''],
      toDate: [''],
    });
   }

  ngOnInit(): void {
  }

  submitForm(value: { fromDate: Date; toDate: Date }): void {
    // for (const key in this.validateForm.controls) {
    //   this.validateForm.controls[key].markAsDirty();
    //   this.validateForm.controls[key].updateValueAndValidity();
    // }
    this.Export.emit([value.fromDate, value.toDate]);
  }

  startValue: Date | null = null;
  endValue: Date | null = null;
  endOpen = false;

  disabledStartDate = (startValue: Date): boolean => {
    if (!startValue || !this.endValue) {
      return false;
    }
    return startValue.getTime() > this.endValue.getTime();
  };

  disabledEndDate = (endValue: Date): boolean => {
    if (!endValue || !this.startValue) {
      return false;
    }
    return endValue.getTime() <= this.startValue.getTime();
  };

  onStartChange(date: Date): void {
    this.startValue = date;
  }

  onEndChange(date: Date): void {
    this.endValue = date;
  }

}
