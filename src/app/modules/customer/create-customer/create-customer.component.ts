import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { CustomerService } from '../service/customer.service';
import { Customer } from '../../../_models/customer.model';
import {FilterItem} from '../../../_models/filteritem.model';
@Component({
  selector: 'app-create-customer',
  templateUrl: './create-customer.component.html',
  styleUrls: ['./../edit-customer/edit-customer.component.less']
})
export class CreateCustomerComponent implements OnInit {
  constructor(private fb: FormBuilder, public customerService: CustomerService) {
    this.validateForm = this.fb.group({
      name: ['', [Validators.required]],
      contractBeginDate: [, [Validators.required]],
      contractEndDate: [, [Validators.required]],
      status: [false],
      contactPoint: ['', [ Validators.required]],
      description: ['']
    });

   }
   newCustomer: Customer;
  isVisible = false;
  @Output() AddCustomer = new EventEmitter<boolean>();
  @Input() cpFilterList: FilterItem[];
  switchValue = false;
  validateForm: FormGroup;

  showModal(): void {
    this.isVisible = true;
  }
  handleCancel(): void {
    this.isVisible = false;
    this.validateForm.reset();
    this.validateForm = this.fb.group({
      name: ['', [Validators.required]],
      contractBeginDate: [, [Validators.required]],
      contractEndDate: [, [Validators.required]],
      status: [false],
      contactPoint: ['', [ Validators.required]],
      description: ['']
    });
  }


  ngOnInit(): void {
  }
  // ngOnChange(): void{
  //   this.validateForm = this.fb.group({
  //     name: ['', [Validators.required]],
  //     contractBeginDate: [, [Validators.required]],
  //     contractEndDate: [, [Validators.required]],
  //     status: [],
  //     contactPoint: ['', [ Validators.required]],
  //     description: ['']
  //   });
  // }
  changeStatus(evt: boolean): void{
    if (evt === true){
      this.validateForm.value.status = 1;
    }
    else {
      this.validateForm.value.status = 0;
    }
  }

  startValue: Date | null = null;
  endValue: Date | null = null;
  endOpen = false;

  disabledStartDate = (startValue: Date): boolean => {
    if (!startValue || !this.endValue) {
      return false;
    }
    return startValue.getTime() > this.endValue.getTime();
  };

  disabledEndDate = (endValue: Date): boolean => {
    if (!endValue || !this.startValue) {
      return false;
    }
    return endValue.getTime() <= this.startValue.getTime();
  };

  onStartChange(date: Date): void {
    this.startValue = date;
  }

  onEndChange(date: Date): void {
    this.endValue = date;
  }

  submitForm(value: { name: string,
                      contractBeginDate: Date
                      contractEndDate: Date,
                      contactPoint: string,
                      status: boolean,
                      description: string}): void {

    // for (const key in this.validateForm.controls) {
    //   this.validateForm.controls[key].markAsDirty();
    //   this.validateForm.controls[key].updateValueAndValidity();
    // }

    this.customerService.createCustomer(value)
                        .subscribe(res => {
                          this.AddCustomer.emit(true);
                          this.isVisible = false;
                          this.validateForm.reset();
                          this.validateForm = this.fb.group({
                            name: ['', [Validators.required]],
                            contractBeginDate: [, [Validators.required]],
                            contractEndDate: [, [Validators.required]],
                            status: [false],
                            contactPoint: ['', [ Validators.required]],
                            description: ['']
                          });
                        });
    }

}
