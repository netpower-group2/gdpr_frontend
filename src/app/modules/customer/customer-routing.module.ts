import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthorizationGuard } from 'src/app/_helpers/authoriztion.guard';
import { CustomerComponent } from './customer.component';
import { RequestManageComponent } from '../request/manage/manage.component';
const routes: Routes = [
  {
    path: '', component: CustomerComponent,
    canActivateChild: [AuthorizationGuard],
    children: [
      {
        path: 'manage',
        component: RequestManageComponent,
        data: {
          allowedRoles: ['0', '3']
        }
      },
      {
        path: '**',
        component: CustomerComponent,
        data: {
          allowedRoles: ['0', '3']
        }

      },
      // {
      //   path: '**',
      //   redirectTo: '',
      //   pathMatch: 'full'
      // },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule { }
