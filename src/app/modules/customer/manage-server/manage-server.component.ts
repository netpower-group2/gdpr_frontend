import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Customer } from '../../../_models/customer.model';
import { Server } from '../../../_models/server.model';
@Component({
  selector: 'app-manage-server',
  templateUrl: './manage-server.component.html',
  styleUrls: ['./manage-server.component.less']
})
export class ManageServerComponent implements OnChanges {
  isVisible = false;
  searchValue: string = '';
  @Input() custom: Customer;
  @Output() VisibleChange = new EventEmitter<boolean>();
  @Input() listServer: Server[];
  @Output() changeServer = new EventEmitter<string[][]>();
  titleTabs = ["Show All", "Owned", "Available"];
  listServersShowAll: Server[] = [];
  listServersOwned: Server[] = [];
  listServersAvailable: Server[] = [];
  listUnOwn: string[] = [];
  listAddOwn: string[] = [];

  validateForm: FormGroup;

  handleCancel(): void {
    this.isVisible = false;
    this.VisibleChange.emit(this.isVisible);
    this.listUnOwn = [];
    this.listAddOwn = [];
  }
  onView(): void {
    this.isVisible = true;
    this.VisibleChange.emit(this.isVisible);
  }
  handleSave(): void {
    this.changeServer.emit([this.listAddOwn, this.listUnOwn, [this.custom.id]]);
    this.listUnOwn = [];
    this.listAddOwn = [];
  }
  constructor(private fb: FormBuilder) {
    this.validateForm = this.fb.group({
      serverName: ['', [Validators.required]],
      ipAddress: ['', [Validators.required]],
      serverStartDate: ['', [Validators.required]],
      serverEndDate: ['', [Validators.required]],
      statusServer: ['', [Validators.required]],
    });
  }

  onSearch(): void {
    this.listServersOwned = [];
    this.listServersAvailable = [];
    this.listServersShowAll = [];
    for (const server of this.listServer) {
      if (server.ipAddress.indexOf(this.searchValue) >= 0) {
        this.listServersShowAll.push(server);
        if (server.customerId === this.custom.id) {
          this.listServersOwned.push(server);
        }
        if (server.customerId === null) {
          this.listServersAvailable.push(server);
        }
      }
    }
  }

  ngOnInit(): void {
    this.listServersOwned = [];
    this.listServersAvailable = [];
    this.listServersShowAll = [];
    for (const a of this.listServer) {
      this.listServersShowAll.push(a);
      if (a.customerId === this.custom.id) {
        this.listServersOwned.push(a);
      }
      if (a.customerId === null) {
        this.listServersAvailable.push(a);
      }
    }
  }
  ngOnChanges(): void {
    this.listServersOwned = [];
    this.listServersAvailable = [];
    this.listServersShowAll = [];
    for (const a of this.listServer) {
      this.listServersShowAll.push(a);
      if (a.customerId === this.custom.id) {
        this.listServersOwned.push(a);
      }
      if (a.customerId === null) {
        this.listServersAvailable.push(a);
      }
    }

  }

  isDisable(serverIdcustomer: string): boolean {
    if ((serverIdcustomer !== this.custom.id && serverIdcustomer != null) || this.custom.status === 0) {
      return true;
    }
    else {
      return false;
    }
  }
  
  isCustomerAvaiable(): boolean {
    if (this.custom.status === 0) {
      return true;
    }
    else {
      return false;
    }
  }

  isCheckbox(serverIdcustomer: string): boolean {
    if (serverIdcustomer === null) {
      return false;
    }
    else {
      return true;
    }
  }

  changeCheckBox(evt: boolean, serverID: string): void {
    if (evt) {
      if (this.listUnOwn.indexOf(serverID) !== -1) {
        this.listUnOwn.splice(this.listUnOwn.indexOf(serverID), 1);
      }
      else {
        this.listAddOwn.push(serverID);
      }
    }
    else {
      if (this.listAddOwn.indexOf(serverID) !== -1) {
        this.listAddOwn.splice(this.listAddOwn.indexOf(serverID), 1);
      }
      else {
        this.listUnOwn.push(serverID);
      }
    }
  }


}
