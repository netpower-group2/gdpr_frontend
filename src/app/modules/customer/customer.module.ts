import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerRoutingModule } from './customer-routing.module';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzCommentModule } from 'ng-zorro-antd/comment';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { CustomerComponent } from './customer.component';
import {CustomerService} from './service/customer.service';
import {EditCustomerComponent} from './edit-customer/edit-customer.component';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzModalModule } from 'ng-zorro-antd/modal';
import {ManageServerComponent} from './manage-server/manage-server.component';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { CreateCustomerComponent } from './create-customer/create-customer.component';
import { ExportCustomerComponent } from './export-customer/export-customer.component';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { CustomerTableComponent } from './customer-table/customer-table.component';
import { CustomerRowTableComponent } from './customer-row-table/customer-row-table.component';

import { NzTreeModule } from 'ng-zorro-antd/tree';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { NzAutocompleteModule } from 'ng-zorro-antd/auto-complete';
import { SearchCustomerComponent } from './search-customer/search-customer.component';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { ScrollingModule } from '@angular/cdk/scrolling';
@NgModule({
  declarations: [ CustomerComponent, EditCustomerComponent, ManageServerComponent, CreateCustomerComponent, ExportCustomerComponent, CustomerTableComponent, CustomerRowTableComponent, SearchCustomerComponent],
  imports: [
    CommonModule,
    CustomerRoutingModule,
    NzLayoutModule,
    NzMenuModule,
    FormsModule,
    NzCardModule,
    NzGridModule,
    NzButtonModule,
    NzDatePickerModule,
    NzListModule,
    NzCommentModule,
    NzAvatarModule,
    NzSwitchModule,
    NzSelectModule,
    NzFormModule,
    NzModalModule,
    ReactiveFormsModule,
    NzTabsModule,
    NzInputModule,
    NzCheckboxModule,
    NzIconModule,
    NzTreeModule,
    NzTableModule,
    NzPaginationModule,
    NzAutocompleteModule,
    NzDropDownModule,
    ScrollingModule

  ],
  providers: [CustomerService]
})
export class CustomerModule { }
