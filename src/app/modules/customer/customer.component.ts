import { Component, OnInit } from '@angular/core';
import { Customer } from '../../_models/customer.model';
import { Router } from '@angular/router';
import { EditCustomerComponent } from './edit-customer/edit-customer.component';
import { CustomerService } from './service/customer.service';
import { CustomerBaseService } from '../../_services/basecustomer.service';
import {FilterItem} from '../../_models/filteritem.model';
import {Server} from '../../_models/server.model';
@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.less']
})
export class CustomerComponent implements OnInit {
  constructor(private router: Router, private customerService: CustomerService) {

  }

  ngOnInit(): void {

  }

}
