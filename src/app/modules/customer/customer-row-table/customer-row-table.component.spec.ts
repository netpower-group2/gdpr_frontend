import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerRowTableComponent } from './customer-row-table.component';

describe('CustomerRowTableComponent', () => {
  let component: CustomerRowTableComponent;
  let fixture: ComponentFixture<CustomerRowTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerRowTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerRowTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
