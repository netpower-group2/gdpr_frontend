import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import{RequestModel} from '../../../_models/request.model';
@Component({
  selector: 'tr[customer-row-item]',
  templateUrl: './customer-row-table.component.html',
  styleUrls: ['./customer-row-table.component.less']
})
export class CustomerRowTableComponent implements OnInit {
  @Input() actor: any;
  @Input() listTitle: any[];
  @Input() nzcheck: boolean;

  @Output() checkevt = new EventEmitter();
  @Output() onEditAction: EventEmitter<any> = new EventEmitter();
  @Output() onViewManage: EventEmitter<any> = new EventEmitter();
 // @Output() onDeleteAction: EventEmitter<string> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }
  onEditItem() {
    this.onEditAction.emit(this.actor);
  }
  onViewEvt(){
    this.onViewManage.emit(this.actor);
  }
}
