import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RequestModel } from '../../../_models/request.model';
import { CustomerService } from '../service/customer.service';

import { FilterItem } from '../../../_models/filteritem.model';
import { Customer } from '../../../_models/customer.model';
import { Server } from '../../../_models/server.model';
import * as moment from 'moment';
import { saveAs } from 'file-saver';

interface ItemData {
  id: string;
}

@Component({
  selector: 'app-customer-table',
  templateUrl: './customer-table.component.html',
  styleUrls: ['./customer-table.component.less']
})
export class CustomerTableComponent implements OnInit {

  dataShow: any[];
  visitSearch: string;
  visible = false;
  checked = false;
  isLoading = false;
  listFilterCP: string[] = [];
  FilterStatusValue: string = null;
  showFilter = false
  indeterminate = false;
  setOfCheckedId = new Set<string>();
  multiList: string = '//';
  multiExport = '';
  listOfFilterStatus = [
    { text: 'Active', value: 'Active' },
    { text: 'InActive', value: 'InActive' }
  ];
  sortStatusList: string[] = [null, null, null, null];
  // ngOnChange(){
  //   this.dataShow = this.data;
  // }
  //-----------------------------------------------
  listOfData: Customer[] = [];

  CPFilterList: FilterItem[] = [];
  listServer: Server[] = [];
  listAddServer: string = '';
  listUnServer: string = '';

  totalData = 100000;
  searchPageKey: string = null;
  searchColumn: string = null;
  searchColumnKey: string = '';
  pageIndex = 1;
  pageSize = 10;
  sortColumn: string = null;
  sortType: string = null;
  filterColumn: string = null;
  filterValue: string = null;
  index: number = 1;
  constructor(private customerService: CustomerService) {

  }


  changePageIndex(evt: number): void {
    this.checked = false;
    this.indeterminate = false;
    this.setOfCheckedId.clear();
    this.pageIndex = evt;
    this.getPageCustomer();
  }

  changePageSize(evt: number): void {
    this.checked = false;
    this.indeterminate = false;
    this.setOfCheckedId.clear();
    this.pageSize = evt;
    this.getPageCustomer();
  }



  searchColumnAction(evt: string[]): void {
    this.checked = false;
    this.indeterminate = false;
    this.setOfCheckedId.clear();
    this.searchColumn = evt[1];
    this.searchColumnKey = evt[0];
    this.getPageCustomer();
  }

  onSortAction(evt: string, column: string): void {
    this.checked = false;
    this.indeterminate = false;
    this.setOfCheckedId.clear();
    if (evt === null) {
      this.sortColumn = null;
      this.sortType = null;
    }
    else {
      this.sortColumn = column;
      this.sortType = evt;
    }
    this.getPageCustomer();
    if (column === 'Customer Name') {
      this.sortStatusList[1] = null;
      this.sortStatusList[2] = null;
      this.sortStatusList[3] = null;

    }
    else if (column === 'Contract Begin Date') {
      this.sortStatusList[0] = null;
      this.sortStatusList[2] = null;
      this.sortStatusList[3] = null;

    }
    else if (column === 'Contract End Date') {
      this.sortStatusList[0] = null;
      this.sortStatusList[1] = null;
      this.sortStatusList[3] = null;

    }
    else if (column === 'Machines') {
      this.sortStatusList[0] = null;
      this.sortStatusList[1] = null;
      this.sortStatusList[2] = null;

    }
  }

  onSearchAction(evt: string): void {
    this.checked = false;
    this.indeterminate = false;
    this.setOfCheckedId.clear();
    this.searchPageKey = evt;
    this.getPageCustomer();

  }

  onFilterAction(evt: string[], column: string): void {
    this.checked = false;
    this.indeterminate = false;
    this.setOfCheckedId.clear();
    // this.filterColumn = column;
    // this.filterValue = evt;
    // this.getPageCustomer();
    if (column === 'Contact Point') {
      this.listFilterCP = evt;
      if (evt.length === 0) {
        if (this.FilterStatusValue != null) {
          this.filterColumn = 'Status';
          this.setFilterValue();
          this.getPageCustomer();
        }
        else {
          this.filterColumn = null;
          this.filterValue = null;
          this.getPageCustomer();
        }
      }
      else {
        if (this.FilterStatusValue != null) {
          this.filterColumn = 'CPandStatus';
          this.setFilterValue();
          this.getPageCustomer();
        }
        else {
          this.filterColumn = 'Contact Point';
          this.setFilterValue();
          this.getPageCustomer();
        }
      }
    }
    else {
      this.FilterStatusValue = evt[0];
      if (evt[0] === null) {
        if (this.listFilterCP.length !== 0) {
          this.filterColumn = 'Contact Point';
          this.setFilterValue();
          this.getPageCustomer();
        }
        else {
          this.filterColumn = null;
          this.filterValue = null;
          this.getPageCustomer();
        }
      }
      else {
        if (this.listFilterCP.length !== 0) {
          this.filterColumn = 'CPandStatus';
          this.setFilterValue();
          this.getPageCustomer();
        }
        else {
          this.filterColumn = 'Status';
          this.setFilterValue();
          this.getPageCustomer();
        }
      }
    }

  }
  
  setFilterValue(): void{
    if(this.listFilterCP.length > 0 && this.FilterStatusValue !== null){
      this.filterValue = '//';
      this.filterValue = this.filterValue + this.FilterStatusValue + '//';
      for (const a of this.listFilterCP){
        this.filterValue = this.filterValue + a + '//';
      }
    }
    else {
      if(this.FilterStatusValue !== null){
        this.filterValue = this.FilterStatusValue;
     }
     else if(this.listFilterCP.length > 0){
       this.filterValue = '//';
       for (const a of this.listFilterCP){
         this.filterValue = this.filterValue + a + '//';
       }
      }
      else{
        this.filterValue = null;
      }
    }
  }

  changeCustomer(evt: boolean): void {
    this.checked = false;
    this.indeterminate = false;
    this.setOfCheckedId.clear();
    if (evt) {
      this.getPageCustomer();
    }
  }
  onChangeServer(evt: string[][]): void {
    this.checked = false;
    this.indeterminate = false;
    this.setOfCheckedId.clear();
    this.listAddServer = '//';
    this.listUnServer = '//';
    for (const str of evt[0]) {
      this.listAddServer = this.listAddServer + str + '//';
    }
    for (const str of evt[1]) {
      this.listUnServer = this.listUnServer + str + '//';
    }
    this.customerService.updateCustomerServer(evt[2][0], this.listAddServer, this.listUnServer)
      .subscribe(res => {
        if (res.success) {
          this.getCustomer();
          this.getServer();
        }
      })
  }

  addCustomer(evt: string): void {
    this.checked = false;
    this.setOfCheckedId.clear();
    if (evt) {
      this.getPageCustomer();
    }
  }

  getCustomer(): void {
    this.index = 1;
    this.isLoading = true;
    this.customerService.getAllCustomer()
      .subscribe(res => {
        this.isLoading = false;
        this.totalData = res.noRecords;
        // tslint:disable-next-line: max-line-length
        this.listOfData = res.customerDetails.map(a => new Customer(`${(this.pageSize * (this.pageIndex - 1)) + this.index++}`, a.id, a.name, a.contactPointId, a.contactPointName, a.contractBeginDate, a.contractEndDate,
          // tslint:disable-next-line: max-line-length
          a.createdBy, a.createdDate, a.description, a.status, a.numberServers, a.updatedBy, a.updatedDate));
      });
  }
  getServer(): void {
    this.customerService.getServer()
      .subscribe(res => {
        this.listServer = res.serverDetails.map(a => new Server(0, a.serverId, a.createdDate, a.createdBy,
          a.updatedDate, a.updatedBy, a.startDate, a.endDate,
          a.nameServer, a.ipAddress, a.customerId, a.status, a.companyName));
      });
  }
  getPageCustomer(): void {
    this.isLoading = true;
    this.index = 1;

    this.customerService.getPageCustomer(this.searchPageKey, this.searchColumn, this.searchColumnKey, this.pageIndex, this.pageSize,
      this.sortColumn, this.sortType, this.filterColumn, this.filterValue)
      .subscribe(res => {
        this.isLoading = false;
        this.totalData = res.noRecords;
        // tslint:disable-next-line: max-line-length
        this.listOfData = res.customerDetails.map(a => new Customer(`${(this.pageSize * (this.pageIndex - 1)) + this.index++}`, a.id, a.name, a.contactPointId, a.contactPointName, a.contractBeginDate, a.contractEndDate,
          // tslint:disable-next-line: max-line-length
          a.createdBy, a.createdDate, a.description, a.status, a.numberServers, a.updatedBy, a.updatedDate));
      });
  }

  onMultiAction(evt: number): void {
    for (const item of this.listOfData) {
      if (this.setOfCheckedId.has(item.id)) {
        this.multiList = this.multiList + item.id + '//';
      }
    }
    this.customerService.updateMultiActionCustomer(evt, this.multiList)
      .subscribe(res => {
        this.multiList = '//';
        this.onAllChecked(false);
        this.getPageCustomer();
      })

  }

  exportRequest(evt: Date[]): void {
    this.multiList = '';
    for (const item of this.listOfData) {
      if (this.setOfCheckedId.has(item.id)) {
        if (this.multiList === '') {
          this.multiList = this.multiList + item.id;
        }
        else {
          this.multiList = this.multiList + ',' + item.id;
        }
      }
    }
    this.customerService.exportRequest(this.multiList, evt[0], evt[1])
      .subscribe(blod => {
        if (blod)
          saveAs(blod, 'CustomerExport.csv');
        else {
        }
      });
  }
  //-----------------------------------------------
  updateCheckedSet(id: string, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }
  normalizeDate(date: string) {
    if (date !== null) {
      return moment(date).format('DD.MM.YYYY');
    }
    else {
      return null;
    }
  }


  onItemChecked(id: string, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  onAllChecked(value: boolean): void {
    this.listOfData.forEach(item => this.updateCheckedSet(item.id, value));
    this.refreshCheckedStatus();
  }



  refreshCheckedStatus(): void {
    this.checked = this.listOfData.every(item => this.setOfCheckedId.has(item.id));
    this.indeterminate = this.listOfData.some(item => this.setOfCheckedId.has(item.id)) && !this.checked;
  }

  reset(): void {
    this.searchColumn = null;
    this.searchColumnKey = '';
    this.getPageCustomer();
  }

  search(): void {
    this.searchColumn = 'Name';
    this.getPageCustomer();
    this.visible = false;
    this.onAllChecked(false);
  }





  ngOnInit(): void {
    this.getCustomer();
    this.customerService.getCP()
      .subscribe(res => {
        this.CPFilterList = res.cpDetails.map(a => new FilterItem(a.name, a.id));

      });
    this.customerService.getServer()
      .subscribe(res => {
        this.listServer = res.serverDetails.map(a => new Server(0, a.serverId, a.createdDate, a.createdBy,
          a.updatedDate, a.updatedBy, a.startDate, a.endDate,
          a.nameServer, a.ipAddress, a.customerId, a.status, a.companyName));
      });
  }
}
