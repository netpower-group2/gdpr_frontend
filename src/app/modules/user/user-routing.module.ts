import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user.component';
import { RequestManageComponent } from '../request/manage/manage.component';

import { AuthorizationGuard } from 'src/app/_helpers/authoriztion.guard';
import { ListUsersComponent } from './list-users/list-users.component';
const routes: Routes = [
  {
    path: '', component: ListUsersComponent,
    canActivateChild: [AuthorizationGuard],
    children: [
      {
        path: '**',
        component: UserComponent,
        data: {
          allowedRoles: ['0']
        }

      },
      // {
      //   path: '**',
      //   redirectTo: '',
      //   pathMatch: 'full'
      // },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
