import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {UserBaseService} from '../../../_services/user.service';
import {User} from '../../../_models/user.model';
import { id_ID } from 'ng-zorro-antd/i18n';
import { PageUserResponse } from 'src/app/_models/pageuser-response';
import { EmailValidator } from '@angular/forms';
import { CreateUserResponse } from 'src/app/_models/createuser-response';
import { UpdateResponse } from 'src/app/_models/update-response';

@Injectable()
export class UserService extends UserBaseService{
  public createUser(email: string, role: number): Observable<CreateUserResponse> {
    return this.baseCreateUser(email, role);
  }

  public updateUser(data: User): Observable<UpdateResponse> {
    return this.baseUpdateUser(data.id, data);
  }

  public getUsers(pageIndex: number, pageSize: number, 
                  sortField: string, sortOrder: string, 
                  filter: Array<{ key: string; value: string[] }>,
                  searchKey: string = null): Observable<PageUserResponse> {
    return this.baseGetUsers(pageIndex, pageSize, sortField, sortOrder, filter, searchKey);
  }

  public multipleChange(status: number, lst: string): Observable<UpdateResponse> {
    return this.baseMultipleChange(status, lst);
  }

  public deleteUser(id: string): Observable<UpdateResponse> {
    return this.baseDeleteUser(id);
  }

  // public deleteUser(id: string): Observable<boolean> {
  //   return this.baseDeleteUser(id);
  // }

}
