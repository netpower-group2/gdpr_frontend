import { Component, OnInit } from '@angular/core';
import { User } from '../../_models/user.model';
import { Router } from '@angular/router';
import { UserService } from './service/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.less']
})

export class UserComponent implements OnInit {
  constructor(private userService: UserService, private router: Router) { }
  
  ngOnInit(): void {
  }
}
