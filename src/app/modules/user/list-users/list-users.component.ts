import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/_models/user.model';
import { UserService } from '../service/user.service';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { NzMessageService } from 'ng-zorro-antd/message';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.less']
})

export class ListUsersComponent implements OnInit {
  visible = false;                        //search box
  searchValue = "";
  checked = false;
  isLoading = false;
  indeterminate = false;
  setOfCheckedId = new Set<string>();
  dataSource: User[] = [];
  pageIndex = 1;
  pageSize = 10;
  sortOrder = "";
  sortField = "";
  total = 10000000;
  filterRole = [
    { text: 'Admin', value: '0' },
    { text: 'User', value: '1' },
    { text: 'DC Member', value: '2' },
    { text: 'Contact Point', value: '3' },
  ];
  filterStatus = [
    { text: 'Active', value: '1' },
    { text: 'In-active', value: '0' }
  ]
  filter: Array<{ key: string, value: string[] }>;

  constructor(private userService: UserService, 
              private router: Router, 
              private nzMessageService: NzMessageService) { }

  ngOnInit(): void {
    this.loadDataFromServer(this.pageIndex, this.pageSize, null, null, []);
  }

  updateCheckedSet(id: string, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }

  addNewUser(event: User): void {
    if (event != null) {
      this.dataSource = [event].concat(this.dataSource);
      if (this.dataSource.length > this.pageSize) {
        var user = this.dataSource.pop();
        this.setOfCheckedId.delete(user.id);
        this.refreshCheckedStatus();
      }
    }
  }

  editUser(event: User): void {
    if (event != null) {
      this.dataSource = [event].concat(this.dataSource);
      if (this.dataSource.length > this.pageSize) {
        let index = this.dataSource.findIndex(x => x.id == event.id);
        this.dataSource[index] = event;
      }
    }
  }

  onCurrentPageDataChange(listOfCurrentPageData: User[]): void {
    this.dataSource = listOfCurrentPageData;
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    this.checked = this.dataSource.every(item => this.setOfCheckedId.has(item.id));
    this.indeterminate = this.dataSource.some(item => this.setOfCheckedId.has(item.id)) && !this.checked;
  }

  onItemChecked(id: string, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  onAllChecked(value: boolean): void {
    this.dataSource.forEach(item => this.updateCheckedSet(item.id, value));
    this.refreshCheckedStatus();
  }

  loadDataFromServer(
    pageIndex: number,
    pageSize: number,
    sortField: string | null,
    sortOrder: string | null,
    filter: Array<{ key: string; value: string[] }>,
    searchKey: string = null
  ): void {
    this.isLoading = true;
    this.userService.getUsers(pageIndex, pageSize, sortField, sortOrder, filter, searchKey).subscribe(data => {
      this.isLoading = false;
      if (data.success) {
        this.total = data.noRecords;
        this.dataSource = data.userDetails.map(u => new User(
          u.id,
          u.createdDate,
          u.firstName,
          u.lastName,
          u.email,
          u.role,
          u.status,
          u.birthday,
          u.isDeleted,
          u.avatar)
        );
        this.setOfCheckedId.clear();
        this.refreshCheckedStatus();
      }
    });
  }

  onQueryParamsChange(params: NzTableQueryParams): void {
    this.pageSize = params.pageSize;
    this.pageIndex = params.pageIndex;
    const currentSort = params.sort.find(item => item.value !== null);
    this.sortField = (currentSort && currentSort.key) || null;
    this.sortOrder = (currentSort && currentSort.value) || null;
    this.filter = params.filter;
    this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter, this.searchValue);
  }

  search(): void {
    this.visible = false;
    this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter, this.searchValue);
  }

  reset(): void {
    this.searchValue = '';
    this.visible = false;
    this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
    this.refreshCheckedStatus();
  }

  multipleAction(status: number): void {
    var arrId = Array.from(this.setOfCheckedId);
    var lstId = arrId.join(',');
    this.userService.multipleChange(status, lstId).subscribe(res => {
      if (res.success) {
        arrId.forEach(element => {
          this.dataSource[this.dataSource.findIndex(u => u.id == element)].status = status;
        });
        this.nzMessageService.success("Updated Successful!");
      }
      else this.nzMessageService.success("Something went wrong!");
    })
  }

  delete(id: string): void {
    this.userService.deleteUser(id).subscribe(res => {
      if (res.success) {
        this.dataSource.splice(this.dataSource.findIndex(x => x.id == id), 1);
        this.setOfCheckedId.delete(id);
        this.nzMessageService.success("Delete successful!");
        this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
      }
      else this.nzMessageService.error("Cannot delete!")
    });
  }
  cancelDelete(): void {

  }
}
