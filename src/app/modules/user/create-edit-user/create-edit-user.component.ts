import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { User } from '../../../_models/user.model';
import { UserService } from '../service/user.service';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
  selector: 'app-create-edit-user',
  templateUrl: './create-edit-user.component.html',
  styleUrls: ['./create-edit-user.component.less']
})
export class CreateEditUserComponent implements OnInit {
  @Input() actionName: string;
  @Input() data: User;
  @Output() createUser = new EventEmitter<User>();
  @Output() editUser = new EventEmitter<User>();
  actionDisplay: string;
  isLoading = false;
  isVisible = false;
  switchValue = false;
  roles = [
    {
      label: 'Administrator',
      value: 0
    },
    {
      label: 'User',
      value: 1
    },
    {
      label: 'DC Member',
      value: 2
    },
    {
      label: 'Contact Point',
      value: 3
    }
  ];
  validateForm: FormGroup;

  showModal(): void {
    this.isVisible = true;
  }

  handleCancel(): void {
    this.isVisible = false;
    if (this.actionName == "create") this.validateForm.reset();
    if (this.actionName == "edit") {
      this.validateForm.setValue({email: this.data.email, role: this.data.role, status: this.data.status})
    }
    this.isLoading = false;
  }

  constructor(private fb: FormBuilder, private userService: UserService, private nzNotificationService: NzNotificationService) {
  }

  ngOnInit(): void {
    this.actionDisplay = (this.actionName == "create") ? "Create New User" : "Edit";
    if (this.actionName == "create"){
      this.validateForm = this.fb.group({
        email: ['', [Validators.email, Validators.required]],
        role: ['', [Validators.required]]
      });
    }
    else {
      this.validateForm = this.fb.group({
        email: [this.data.email, [Validators.email, Validators.required]],
        role: [this.data.role, [Validators.required]],
        status: [this.data.status, [Validators.required]]
      });
    }
  }

  submitForm(value: { email: string; role: number; status: boolean }): void {
    for (const key in this.validateForm.controls) {
      this.validateForm.controls[key].markAsDirty();
      this.validateForm.controls[key].updateValueAndValidity();
    }
    this.isLoading = true;
    if (this.actionName == "create") {
      this.userService.createUser(value.email, value.role).subscribe(res => {
        if (res.success) {
          res.userDetail.fullName = "";
          this.nzNotificationService.success('Create successful!', `User ${res.userDetail.email} has been added to the database. Tell them to check their email for password.`);
          this.createUser.emit(res.userDetail);
          
          this.isLoading = false;
          this.isVisible = false;
          this.validateForm.reset();
        }
      },
      error => {
        this.nzNotificationService.error("Create failed!", "Something wrong happened! Please try again! Maybe the user you're trying to create has already had an account");
        this.createUser.emit(null);
        this.isLoading = false;
      });
    } 
    else {
      this.data.email = value.email;
      this.data.role = value.role;
      this.data.status = value.status ? 1 : 0;
      this.userService.updateUser(this.data).subscribe(res => {
        if (res.success) this.createUser.emit(this.data);
        this.nzNotificationService.success("Update successful!", "Changes have been saved!")
        this.isVisible = false;
      },
      error => {
        this.nzNotificationService.error("Update failed!", "Something wrong happened!")
      });
      this.isLoading = false;
    }
    
  }

  confirmValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { error: true, required: true };
    } else if (control.value !== this.validateForm.controls.password.value) {
      return { confirm: true, error: true };
    }
    return {};
  };

}
