import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ConstantPool } from '@angular/compiler';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {
  constructor(
    private jwtHelperService: JwtHelperService,
    private router: Router
  ) { }

  isAuthorized(allowedRoles: string[]): boolean {
    if (allowedRoles == null || allowedRoles.length === 0) {
      return true;
    }

    if (localStorage.getItem('userRole') == null) {
      return false;
    }

    const token = localStorage.getItem('userRole');
    const authToken = JSON.parse(token).authToken;
    const decodeToken = this.jwtHelperService.decodeToken(authToken);

    if (!decodeToken) {
      return false;
    }

    if (this.jwtHelperService.isTokenExpired(token)) {
      this.router.navigate(['/login'])
      localStorage.removeItem('userRole');
      return false;
    }

    return allowedRoles.includes(decodeToken.rol);
  }
}


