import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class PingJWTService {

  constructor(
    private jwtHelperService: JwtHelperService,
  ) { }

  pingJWT(){
    const token = localStorage.getItem('userRole');
    const expirationDate = this.jwtHelperService.getTokenExpirationDate(token);
    const now: Date = new Date();
    const timeLeft = moment(expirationDate).diff(moment(now), 'minutes')
    return timeLeft;
  }
 
}
