import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CrudService } from './crud.service';
import { RequestModel } from '../_models/request.model';
import { PageRequestResponse } from '../_models/pagerequest-response';
import { CreateRequestResponse } from '../_models/createrequest-response';
import { UpdateResponse } from '../_models/update-response';
import { ServerSelect } from '../_models/serverSelect.model';
import { PageServerResponse } from '../_models/pageserver-response';

@Injectable()
export class RequestBaseService extends CrudService {
  public baseCreateRequest(data: any): Observable<CreateRequestResponse> {
    return this.post('/Request/create', data);
  }

  public baseUpdateRequest(id: string, data: any): Observable<boolean> {
    return this.put(`/Request/${id}`, data);
  }

  public baseManageRequest(answer: string, status: number, requestId: string): Observable<boolean> {
    return this.put(`/Request/manage`, { answer, status, requestId });
  }

  public baseGetRequest(
    PageNo: number, PageSize: number,
    SortColumn: string, SortOrder: string,
    filters: Array<{ key: string; value: string[] }>,
    SearchColumn: string,
    SearchKey: string
  ): Observable<PageRequestResponse> {
    const field = filters.filter(x => x.value.length)[0];
    const FilterColumn = field ? field.key : null;
    const FilterKey = field ? field.value.join(',') : null;
    if (SearchColumn === '') {
      SearchColumn = null;
    }
    return this.post('/Request', { PageNo, PageSize, SortOrder, SortColumn, FilterColumn, FilterKey, SearchColumn, SearchKey });
  }

  public baseGetRequestByRequestId(id: string): Observable<RequestModel> {
    return this.get(`/Request/${id}`);
  }

  public baseGetComment(RequestId: string, PageNo: number, PageSize: number, ParentCommentId: string): Observable<boolean> {
    return this.post(`/Comment/paging`, { RequestId, PageNo, PageSize, ParentCommentId });
  }

  public baseCreateComment(requestId: string, replyFor: string, content: string): Observable<boolean> {
    return this.post(`/Comment/create`, { requestId, replyFor, content });
  }

  public baseGetHistoryLog(RequestId: string): Observable<boolean> {
    return this.get(`/HistoryLog/${RequestId}`);
  }

  public baseMultipleChange(Status: number, IdList: string): Observable<UpdateResponse> {
    return this.post('/Request/multiple', { Status, IdList });
  }

  public baseGetAllServer(): Observable<PageServerResponse> {
    return this.get('/Server');
  }

  public baseLove(requestId: string, isLove: number): Observable<any> {
    return this.post('/Comment/love', {requestId, isLove});
  }

}
