import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CrudService {
  baseUrl: string = environment.apiUrl;

  constructor(
    public http: HttpClient,
  ) {}

  getToken() {
    let token = localStorage.getItem('userRole');
    return JSON.parse(token).authToken;
  }

  resolvedOption() {
    let option: any = {};
    option = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json; charset=utf-8',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + this.getToken()
      }),
    }
    return option;
  }

  resolvedOptionForExport() {
    let option: any = {};
    option = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json; charset=utf-8',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + this.getToken()
      }),
      responseType: 'blob' as 'json'
    }
    return option;
  }

  protected get(url: string): Observable<any> {
    return this.http.get(this.baseUrl + url, this.resolvedOption());
  }

  protected post(url: string, data: any): Observable<any> {
    return this.http.post(this.baseUrl + url, data, this.resolvedOption());
  }

  protected put(url: string, data: any): Observable<any> {
    return this.http.put(this.baseUrl + url, data, this.resolvedOption());
  }

  protected delete(url: string): Observable<any> {
    return this.http.delete(this.baseUrl + url, this.resolvedOption());
  }

  protected postExport(url: string, data: any): Observable<any> {
    return this.http.post(this.baseUrl + url, data, this.resolvedOptionForExport());
  }

}
