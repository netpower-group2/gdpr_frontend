import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../_models/user.model';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';


import { environment } from '../../environments/environment'
import { ForgotPasswordResponse } from '../_models/forgotpassword-response';


@Injectable({
  providedIn: 'root'
})

export class AuthenticationService {
    private userSubject: BehaviorSubject<User>;
    public user: Observable<User>;

    httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json; charset=utf-8',
        // 'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT',
        // 'Access-Control-Allow-Origin': '*'
      }),
    }

    constructor(
      private router: Router,
      private http: HttpClient,
    ){
      this.userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('userRole')));
      this.user = this.userSubject.asObservable();
    }

    public get userValue(): User {
      return this.userSubject.value;
    }

    login(email, password) {
      return this.http.post<User>(`${environment.apiUrl}/Auth/login`, { email, password }, this.httpOptions)
        .pipe(map(user => {
            localStorage.setItem('userRole', JSON.stringify(user));
            this.userSubject.next(user);
            return user;
        }));
    }

    forgotPassword(email: string): Observable<ForgotPasswordResponse> {
      return this.http.post<ForgotPasswordResponse>(`${environment.apiUrl}/Auth/forgot`, { email });
    }
}


