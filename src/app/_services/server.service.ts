import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {CrudService} from './crud.service';
import {Server} from '../_models/server.model';
import { PageServerResponse } from '../_models/pageserver-response';
import { CreateServerResponse } from '../_models/createserver-response';
import { UpdateResponse } from '../_models/update-response';

@Injectable()
export class ServerBaseService extends CrudService{
  public baseCreateServer(nameServer: string, ipAddress: string, customerId: string, status: number, startDate : string, endDate: string ): Observable<CreateServerResponse> {
    
    return this.post('/Server/create', {nameServer,ipAddress,customerId,status,startDate,endDate});
  }

  public baseUpdateServer(serverId, nameServer,ipAddress,customerId,status,startDate,endDate): Observable<UpdateResponse> {
    return this.put(`/server/${serverId}`, {nameServer,ipAddress,customerId,status,startDate,endDate});
  }

  public baseGetAllServer(): Observable<PageServerResponse> {
    
    return this.get('/Server');
  }

  public baseGetServerById(id: string): Observable<Server> {
    return this.get(`/server/${id}`);
  }

  public baseDeleteServer(id: string): Observable<boolean> {
    return this.delete(`/server/${id}`);
  }
  public baseMultipleChange(Status: number, IdList: string): Observable<UpdateResponse> {
    return this.post('/Server/multiple', { Status, IdList })
  }
  public baseExportServer(Id: string,fromDate: string,toDate:string,requester:string,approver:string): Observable<any> {
      if(fromDate=="") fromDate=null;
      if(toDate=="") toDate=null;
      if(requester=="") requester=null;
      if(approver=="") approver=null;
    return this.postExport('/Server/export', { Id, fromDate,toDate,requester,approver });
  }

}
