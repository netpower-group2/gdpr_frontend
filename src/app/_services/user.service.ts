import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CrudService } from './crud.service';
import { User } from '../_models/user.model';
import { PageUserResponse } from '../_models/pageuser-response';
import { HttpParams } from '@angular/common/http';
import { CreateUserResponse } from '../_models/createuser-response';
import { UpdateResponse } from '../_models/update-response';
import { GetUserResponse } from '../_models/getuser-response';
import { ForgotPasswordResponse } from '../_models/forgotpassword-response';


@Injectable()
export class UserBaseService extends CrudService{
  public baseCreateUser(email: string, role: number): Observable<CreateUserResponse> {
    return this.post('/Register', {email, role});
  }

  public baseUpdateUser(id: string, data: User): Observable<UpdateResponse> {
    return this.put(`/User/${id}`, data);
  }

  public baseGetUsers(PageNo: number, PageSize: number, 
                      SortColumn: string, SortOrder: string, 
                      filters: Array<{ key: string; value: string[] }>,
                      SearchKey: string): Observable<PageUserResponse> {
    // let params = new HttpParams()
    //   .append('PageNo', `${pageIndex}`)
    //   .append('PageSize', `${pageSize}`)
    //   .append('SortColumn', `${sortField}`)
    //   .append('SortOrder', `${sortOrder}`);
    //   filters.forEach(filter => {
    //     filter.value.forEach(value => {
    //       params = params.append(filter.key, value);
    //     });
    //   });
    var field = filters.filter(x => x.value.length)[0];
    var FilterColumn = field ? field.key : null;
    var FilterKey = field ? field.value.join(',') : null;
    var SearchColumn = SearchKey ? 'Email' : null;
    return this.post('/User', { PageNo, PageSize, SortOrder, SortColumn, FilterColumn, FilterKey, SearchColumn, SearchKey });
  }

  public baseGetInfoUser(): Observable<GetUserResponse> {
    return this.get('/User/info');
  }

  public baseUploadAvatar(UploadAvatar: string): Observable<UpdateResponse> {
    return this.post('/Account/upload', { UploadAvatar });
  }

  public baseUpdateInfo(FirstName: string, LastName: string, Birthday: Date): Observable<UpdateResponse> {
    return this.post('/Account/changeinfo', { FirstName, LastName, Birthday });
  }

  public baseMultipleChange(Status: number, IdList: string): Observable<UpdateResponse> {
    return this.post('/User/multiple', { Status, IdList })
  }

  // public baseGetPageUsers(PageNo: number, PageSize: number): Observable<PageUserResponse> {
  //   return this.post('/User', {PageNo, PageSize});
  // }

  public baseDeleteUser(id: string): Observable<UpdateResponse> {
    return this.delete(`/User/${id}`);
  }

  public baseChangePass(OldPassword: string, NewPassword: string): Observable<UpdateResponse> {
    return this.post('/Account/changepass', { OldPassword, NewPassword });
  }

}
