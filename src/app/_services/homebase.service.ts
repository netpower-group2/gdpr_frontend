import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {CrudService} from './crud.service';

@Injectable()
export class HomeBaseService extends CrudService{


  public baseGetRequestStatistic(): Observable<any> {
    return this.get(`/Home`);
  }

}
