import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {CrudService} from './crud.service';
import {Customer} from '../_models/customer.model';
import {PageCustomerResponse} from '../_models/pagecustomer-response';
import {CreateCustomerResponse} from '../_models/createcustomer-response';
import {CPResponse} from '../_models/cp-response';
import {PageServerResponse} from '../_models/pageserver-response';
import {BaseUpdateResponse} from '../_models/baseupdate-response';

@Injectable()
export class CustomerBaseService extends CrudService{
  public baseCreateCustomer(data: { name: string,
                                    contractBeginDate: Date,
                                    contractEndDate: Date,
                                    contactPoint: string,
                                    status: boolean,
                                    description: string}): Observable<CreateCustomerResponse> {

    return this.post('/Customer/create', data);
  }

  public baseUpdateCustomer(id: string,  value: { name: string,
                                                  contractBeginDate: Date,
                                                  contractEndDate: Date,
                                                  contactPoint: string,
                                                  status: boolean,
                                                  description: string}): Observable<boolean> {
    return this.put(`/Customer/${id}`, value);
  }

  public baseGetAllCustomer(): Observable<PageCustomerResponse> {
    return this.get('/Customer');
  }

  public baseGetCP(): Observable<CPResponse> {
    return this.get('/Customer/cp');
  }

  public baseGetServer(): Observable<PageServerResponse> {
    return this.get('/Server');
  }

  public baseGetPageCustomer(searchPageKey: string, searchColumn: string, searchColumnKey: string,
                             pageNo: number, pageSize: number, sortColumn: string, sortType: string,
                             filterColumn: string, filterValue: string): Observable<PageCustomerResponse> {
    // tslint:disable-next-line: max-line-length
    return this.post('/Customer', {searchPageKey, searchColumn, searchColumnKey, pageNo, pageSize, sortColumn, sortType, filterColumn, filterValue});
  }

  public baseExportRequestByCustomer(id: string, fromDate: Date, toDate: Date): Observable<Blob> {
// tslint:disable-next-line: max-line-length
return this.postExport('/Customer/export', {id, fromDate, toDate});
}

  public baseMultiActionCustomer(status: number,  idList: string): Observable<BaseUpdateResponse> {
// tslint:disable-next-line: max-line-length
return this.post('/Customer/multiple', {status, idList});
}

  public baseUpdateCustomerServer(customerId: string, listServerAddOwn: string, listServerUnOwn: string): Observable<BaseUpdateResponse> {
    // tslint:disable-next-line: max-line-length
    return this.post('/Customer/updateServer', {customerId, listServerAddOwn, listServerUnOwn});
  }

  public baseGetCustomerById(id: string): Observable<Customer> {
    return this.get(`/Customer/${id}`);
  }

  public baseDeleteCustomer(id: string): Observable<boolean> {
    return this.delete(`/Customer/${id}`);
  }

}
