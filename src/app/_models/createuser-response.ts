import { User } from './user.model';

export class CreateUserResponse {
  userDetail: User;
  success: boolean;
}