import { Server } from './server.model';

export class CreateServerResponse {
  serverDetail: Server;
  success: boolean;
}