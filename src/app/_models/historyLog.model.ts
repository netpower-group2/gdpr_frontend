export class HistoryLog {
    modifiedDate: string;
    modifier: string;
    fileContent: string;
    logContent: string;

    constructor(
        modifiedDate: string,
        modifier: string,
        fileContent: string,
        logContent: string
    ) {
        this.modifiedDate = modifiedDate;
        this.modifier = modifier;
        this.fileContent = fileContent;
        this.logContent = logContent;
    }
}

