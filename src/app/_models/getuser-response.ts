import { User } from './user.model';

export class GetUserResponse {
  userInfo: User;
  success: boolean;
}