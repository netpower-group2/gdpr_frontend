import * as moment from 'moment';

export class User {
  id: string;
  createdDate: string;
  firstName: string;
  lastName: string;
  email: string;
  role: number;
  status: number;
  birthday: string;
  isDeleted: boolean;
  avatar: string;
  fullName: string;

  constructor(
    id: string,
    createdDate: string,
    firstName: string,
    lastName: string,
    email: string,
    role: number,
    status: number,
    birthday: string,
    isDeleted: boolean,
    avatar: string
  ) {
    this.id = id;
    this.createdDate = createdDate ? this.normalizeDate(createdDate) : null;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.role = role;
    this.status = status;
    this.birthday = birthday;
    this.isDeleted= isDeleted;
    this.avatar = avatar;
    this.fullName = (this.firstName ? this.firstName + " " : "") + (this.lastName ? this.lastName : "");
  }

  normalizeDate(date: string): string {
    return moment(date).format('DD.MM.YYYY - h:mm A')
  }
}
