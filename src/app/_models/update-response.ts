export class UpdateResponse {
  success: boolean;
  errors: any;
}