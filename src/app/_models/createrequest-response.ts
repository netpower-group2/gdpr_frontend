import { RequestModel } from './request.model';

export class CreateRequestResponse {
  resultCreate: RequestModel;
  success: boolean;
}