import * as moment from 'moment';

export class RequestModel {
  createdByName: string;
  createdDate: string;
  updatedByName: string;
  updatedDate: string;
  answer: string;
  description: string;
  fromDate: string;
  requestId: string;
  numId: string;
  serverId: string;
  serverIpAddress: string;
  serverName: string;
  status: number;
  title: string;
  toDate: string;


  constructor(
    createdByName: string,
    createdDate: string,
    updatedByName: string,
    updatedDate: string,
    description: string,
    fromDate: string,
    requestId: string,
    numId: string,
    serverId: string,
    serverIpAddress: string,
    serverName: string,
    status: number,
    title: string,
    toDate: string,
    answer: string
  ) {
    this.createdByName = createdByName;
    this.createdDate = this.normalizeDate(createdDate);
    this.updatedByName = updatedByName;
    this.updatedDate = updatedDate == null ? null : this.normalizeDate(updatedDate);
    this.description = description == null ? '' : description;
    this.fromDate = this.normalizeDate(fromDate);
    this.requestId = requestId;
    this.numId = numId;
    this.serverId = serverId;
    this.serverIpAddress = serverIpAddress;
    this.serverName = serverName;
    this.status = status;
    this.title = title == null ? '' : title;
    this.toDate = this.normalizeDate(toDate);
    this.answer = answer == null ? '' : answer;
  }

  normalizeDate(date: string): string {
    return moment(date).format('DD.MM.YYYY - h:mmA')
  }
}
