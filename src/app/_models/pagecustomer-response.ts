import { Customer } from './customer.model';

export class PageCustomerResponse {
  noRecords: number;
  customerDetails: Customer[] = [];
  error: any;
  success: boolean;
  message: any;
}
