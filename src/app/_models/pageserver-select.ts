import { ServerSelect } from './serverSelect.model';

export class PageServerResponse {
  serverDetails: ServerSelect[] = [];
  errors: any;
  success: boolean;
}