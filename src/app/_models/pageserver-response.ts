import { Server } from './server.model';

export class PageServerResponse {
  serverDetails: Server[] = [];
  errors: any;
  success: boolean;
  message: any;
}