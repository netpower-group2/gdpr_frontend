import { User } from './user.model';

export class PageUserResponse {
  userDetails: User[] = [];
  noRecords: number;
  success: boolean;
}