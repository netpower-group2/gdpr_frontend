export class BaseUpdateResponse{
  errors: any;
  success: boolean;
  message: any;
}
