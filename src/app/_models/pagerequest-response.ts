import { RequestModel } from './request.model';

export class PageRequestResponse {
  requestDetails: RequestModel[] = [];
  noRecords: number;
  success: boolean;
}
