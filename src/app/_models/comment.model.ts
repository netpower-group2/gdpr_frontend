export class Comment {
	IdComment: string;
	CreatedDate: Date;
	CreatedBy: string;
	CommentContent: string;
	IdReplyComment: string;
	IdRequest: string;
}

