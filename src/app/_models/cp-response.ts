import { ContactPointModel } from './contactpoint.model';

export class CPResponse {
  cpDetails: ContactPointModel[] = [];
  errors: any;
  success: boolean;
  message: any;
}
