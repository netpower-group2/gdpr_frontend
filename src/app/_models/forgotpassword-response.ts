export class ForgotPasswordResponse {
    success: boolean;
    errors: any;
}