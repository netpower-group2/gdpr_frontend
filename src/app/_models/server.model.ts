
export class Server  {
      index: number;
      serverId: string;
      createdDate: string;
      createdBy: string;
      updatedDate: string;
      updatedBy: string;
      startDate: string;
      endDate: string;
      nameServer: string;
      ipAddress: string;
      customerId: string;
      status: number;
      companyName: string;
  constructor(
      index: number,
      serverId: string,
      createdDate: string,
      createdBy: string,
      updatedDate: string,
      updatedBy: string,
      startDate: string,
      endDate: string,
      nameServer: string,
      ipAddress: string,
      customerId: string,
      status: number,
      companyName: string,
  ){
      this.index = index;
      this.serverId = serverId;
      this.createdDate =createdDate; 
      this.createdBy =createdBy;
      this.updatedDate=updatedDate;
      this.updatedBy=updatedBy;
      this.startDate=startDate;
      this.endDate=endDate;
      this.nameServer=nameServer;
      this.ipAddress=ipAddress;
      this.customerId=customerId;
      this.status=status;
      this.companyName=companyName;
  }

  public getvalue(indata: string): any{
    if (indata === 'Id'){
      return this.serverId;
    }
    else if (indata === 'Server'){
      return this.nameServer;
    }
    else if (indata === 'IP Address'){
      return this.ipAddress;
    }
    else if (indata === 'Start Date'){
      return this.startDate;
    }
    else if (indata === 'End Date'){
      return this.endDate;
    }
    else if (indata === 'Owner'){
      return this.companyName;
    }
    else if (indata === 'Status'){
      return this.status;
    }
    else {return null; }
  }
}
