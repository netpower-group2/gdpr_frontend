export class Base {
    id: string;
    createDate: Date;
    createBy: string;
    updateDate: Date;
    updateBy: string;
    deleteDate: Date;
    deleteBy: string;
    isDelete: boolean;
    statusCode: number;
}
