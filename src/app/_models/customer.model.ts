export class Customer {
  index?: string;
  id: string;
  name: string;
  contactPointId: string;
  contactPointName: string;
  contractBeginDate: Date;
  contractEndDate: Date;
  createdBy: string;
  createdDate: Date;
  description: string;
  status: number;
  numberServers: number;
  updatedBy: string;
  updatedDate: Date;
  constructor(
    index: string,
    id: string,
    name: string,
    contactPointId: string,
    contactPointName: string,
    contractBeginDate: Date,
    contractEndDate: Date,
    createdBy: string,
    createdDate: Date,
    description: string,
    status: number,
    numberServers: number,
    updatedBy: string,
    updatedDate: Date) {
    this.index = index;
    this.id = id;
    this.name = name;
    this.contactPointId = contactPointId;
    this.contactPointName = contactPointName;
    this.contractBeginDate = contractBeginDate;
    this.contractEndDate = contractEndDate;
    this.createdBy = createdBy;
    this.createdDate = createdDate;
    this.description = description;
    this.status = status;
    this.numberServers = numberServers;
    this.updatedBy = updatedBy;
    this.updatedDate = updatedDate;
  }

  public getvalue(indata: string): any {
    if (indata === 'Id') {
      return this.id;
    }
    else if (indata === 'Customer Name') {
      return this.name;
    }
    else if (indata === 'Contact Point') {
      return this.contactPointName;
    }
    else if (indata === 'Contract Begin Date') {
      return this.contractBeginDate;
    }
    else if (indata === 'Contract End Date') {
      return this.contractEndDate;
    }
    else if (indata === 'Decription') {
      return this.description;
    }
    else if (indata === 'Status') {
      return this.status;
    }
    else if (indata === 'totalMachine') {
      return this.numberServers;
    }
    else { return null; }
  }
  public isActive(): boolean{
    if ( this.status === 1) {
      return true;
    }
    else {
      return false;
    }
  }
}
