
export class ServerSelect {
    serverId: string;
    nameServer: string;
    ipAddress: string;
    constructor(
        serverId: string,
        nameServer: string,
        ipAddress: string,
    ) {
        this.serverId = serverId;
        this.nameServer = nameServer;
        this.ipAddress = ipAddress;
    }
}
