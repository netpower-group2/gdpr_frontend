import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthorizationGuard } from './_helpers/authoriztion.guard';
const routes: Routes = [
  {

    path: '',
    canActivateChild: [AuthorizationGuard],
    children: [
      {
        path: 'login',
        loadChildren: () => import('./modules/login/login.module').then(m => m.LoginModule),
        data: {}
      },
      {
        path: 'forgot-password',
        loadChildren: () => import('./modules/forgot-password/forgot-password.module').then(m => m.ForgotPasswordModule),
        data: {}
      },
      {
        path: '',
        loadChildren: () => import('./modules/home/home.module').then(m => m.HomeModule),
        data: {}
      },
      {
        path: '**',
        redirectTo: 'login',
        pathMatch: 'full'
      },
    ]
  },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
